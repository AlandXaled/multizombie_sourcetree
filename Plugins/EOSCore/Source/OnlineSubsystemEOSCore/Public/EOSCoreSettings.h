/**
* Copyright (C) 2017-2021 | eelDev
*
* Official SteamCore Documentation: https://eeldev.com
*/

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include <Engine/DataAsset.h>
#include "EOSCoreSettings.generated.h"

#pragma once

#define INI_SECTION TEXT("/Script/OnlineSubsystemEOSCore.EOSCoreSettings")

inline FString StripQuotes(FString& Source)
{
	if (Source.StartsWith(TEXT("\"")))
	{
		return Source.Mid(1, Source.Len() - 2);
	}
	return Source;
}

inline bool ContainsWhitespace(const FString& Source)
{
	for (const TCHAR& IterChar : Source)
	{
		if (FChar::IsWhitespace(IterChar))
		{
			return true;
		}
	}
	return false;
}

inline bool IsAnsi(const FString& Source)
{
	for (const TCHAR& IterChar : Source)
	{
		if (!FChar::IsPrint(IterChar))
		{
			return false;
		}
	}
	return true;
}

inline bool IsHex(const FString& Source)
{
	for (const TCHAR& IterChar : Source)
	{
		if (!FChar::IsHexDigit(IterChar))
		{
			return false;
		}
	}
	return true;
}

USTRUCT(BlueprintType)
struct ONLINESUBSYSTEMEOSCORE_API FEOSConfig
{
	GENERATED_BODY()
public:
	FEOSConfig()
		: ConfigName(FString("DefaultConfig"))
		, EncryptionKey(FString("1111111111111111111111111111111111111111111111111111111111111111"))
		, bEnableRTC(true)
	{
	}

public:
	// Name of the Configuration
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	FString ConfigName;
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	FString ProductId;
	// Your SandboxId found in the EOS Developer Portal
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	FString SandboxId;
	// Your DeploymentId found in the EOS Developer Portal
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	FString DeploymentId;
	// Your ClientId found in the EOS Developer Portal
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	FString ClientId;
	// Your ClientSecret found in the EOS Developer Portal
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	FString ClientSecret;
	// Your EncryptionKey, must be of length 64
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	FString EncryptionKey;
	/** RTC options. Setting to FALSE will disable RTC features (e.g. voice) */
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	bool bEnableRTC;
public:
	void ParseRawArrayEntry(FString& RawLine);
};

UCLASS(config = Engine, defaultconfig, meta = (DisplayName = "EOSCore Plugin"))
class ONLINESUBSYSTEMEOSCORE_API UEOSCoreSettings : public UDeveloperSettings
{
	GENERATED_BODY()
public:
	UEOSCoreSettings();
public:
	// Default EOS Configuration Name
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	FString DefaultConfigurationName;
	// Used to throttle EOS services tick rate
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	int32 TickBudgetInMilliseconds;
	// Client Id used on IOS devices
	UPROPERTY(config, EditAnywhere, Category = "EOSCore", meta = (DisplayName = "IOS Client Id"))
	FString IOSClientId;
	// Client Id used on Android devices
	UPROPERTY(config, EditAnywhere, Category = "EOSCore", meta = (DisplayName = "Android Client Id"))
	FString AndroidClientId;
	UPROPERTY(config, EditAnywhere, Category = "EOSCore")
	TArray<FEOSConfig> EOSConfigurations;
public:
	static bool GetEOSConfig(const FString& ConfigName, FEOSConfig& OutSettings);
private:
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	virtual FName GetCategoryName() const override;
#endif
};

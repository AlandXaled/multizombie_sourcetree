/**
* Copyright (C) 2017-2021 | eelDev
*
* Official EOSCore Documentation: https://eeldev.com
*/

#pragma once

#include "CoreMinimal.h"
#include "OnlineSubsystemTypesEOSCore.h"
#include "NboSerializer.h"

class FNboSerializeToBufferEOS : public FNboSerializeToBuffer
{
public:
	FNboSerializeToBufferEOS() :
		FNboSerializeToBuffer(512)
	{
	}

	FNboSerializeToBufferEOS(uint32 Size) :
		FNboSerializeToBuffer(Size)
	{
	}

	friend inline FNboSerializeToBufferEOS& operator<<(FNboSerializeToBufferEOS& Ar,
	                                                   const FOnlineSessionInfoEOSCore& SessionInfo)
	{
		check(SessionInfo.m_HostAddr.IsValid());
		Ar << SessionInfo.m_SessionId;
		Ar << *SessionInfo.m_HostAddr;
		return Ar;
	}

	friend inline FNboSerializeToBufferEOS& operator<<(FNboSerializeToBufferEOS& Ar, const FUniqueNetIdEOSCore& UniqueId)
	{
		Ar << UniqueId.UniqueNetIdStr;
		return Ar;
	}

	friend inline FNboSerializeToBufferEOS& operator<<(FNboSerializeToBufferEOS& Ar, const FUniqueNetIdString& UniqueId)
	{
		Ar << UniqueId.UniqueNetIdStr;
		return Ar;
	}
};

class FNboSerializeFromBufferEOS : public FNboSerializeFromBuffer
{
public:
	FNboSerializeFromBufferEOS(uint8* Packet, int32 Length) :
		FNboSerializeFromBuffer(Packet, Length)
	{
	}

	friend inline FNboSerializeFromBufferEOS& operator>>(FNboSerializeFromBufferEOS& Ar,
	                                                     FOnlineSessionInfoEOSCore& SessionInfo)
	{
		check(SessionInfo.m_HostAddr.IsValid());
		Ar >> SessionInfo.m_SessionId;
		Ar >> *SessionInfo.m_HostAddr;
		return Ar;
	}

	friend inline FNboSerializeFromBufferEOS& operator>>(FNboSerializeFromBufferEOS& Ar, FUniqueNetIdEOSCore& UniqueId)
	{
		Ar >> UniqueId.UniqueNetIdStr;
		return Ar;
	}

	friend inline FNboSerializeFromBufferEOS& operator>>(FNboSerializeFromBufferEOS& Ar, FUniqueNetIdString& UniqueId)
	{
		Ar >> UniqueId.UniqueNetIdStr;
		return Ar;
	}
};

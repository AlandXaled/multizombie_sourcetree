/**
* Copyright (C) 2017-2021 | eelDev
*
* Official EOSCore Documentation: https://eeldev.com
*/

#pragma once

#include "CoreMinimal.h"
#include "eos_lobby_types.h"
#include "Core/EOSHelpers.h"
#include "EOSLobbyTypes.generated.h"

class UCoreLobby;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		ENUMS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
/** Permission level gets more restrictive further down */
UENUM(BlueprintType)
enum class EEOSELobbyPermissionLevel : uint8
{
	/** Anyone can find this lobby as long as it isn't full */
	EOS_LPL_PUBLICADVERTISED = 0,
	/** Players who have access to presence can see this lobby */
	EOS_LPL_JOINVIAPRESENCE = 1,
	/** Only players with invites registered can see this lobby */
	EOS_LPL_INVITEONLY = 2
};


/** Advertisement properties for a single attribute associated with a lobby */
UENUM(BlueprintType)
enum class EEOSELobbyAttributeVisibility : uint8
{
	/*&, data is visible outside the lobby */
	EOS_LAT_PUBLIC = 0,
	/** Only members in the lobby can see this data */
	EOS_LAT_PRIVATE = 1
};

/** Various types of lobby member updates */
UENUM(BlueprintType)
enum class EEOSELobbyMemberStatus : uint8
{
	/** The user has joined the lobby */
	EOS_LMS_JOINED = 0,
	/** The user has explicitly left the lobby */
	EOS_LMS_LEFT = 1,
	/** The user has unexpectedly left the lobby */
	EOS_LMS_DISCONNECTED = 2,
	/** The user has been kicked from the lobby */
	EOS_LMS_KICKED = 3,
	/** The user has been promoted to lobby owner */
	EOS_LMS_PROMOTED = 4,
	/** The lobby has been closed and user has been removed */
	EOS_LMS_CLOSED = 5
};


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		STRUCTS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
/** Handle to a single lobby */
USTRUCT(BlueprintType)
struct FEOSHLobbyDetails
{
	GENERATED_BODY()
public:
	EOS_HLobbyDetails Handle;
public:
	explicit FEOSHLobbyDetails()
		: Handle(nullptr)
	{
	}

	FEOSHLobbyDetails(const EOS_HLobbyDetails& data)
		: Handle(data)
	{
	}

public:
	operator EOS_HLobbyDetails() const
	{
		return Handle;
	}
};

/** Handle to a lobby modification object */
USTRUCT(BlueprintType)
struct FEOSHLobbyModification
{
	GENERATED_BODY()
public:
	EOS_HLobbyModification Handle;
public:
	explicit FEOSHLobbyModification()
		: Handle(nullptr)
	{
	}

	FEOSHLobbyModification(const EOS_HLobbyModification& data)
		: Handle(data)
	{
	}

public:
	operator EOS_HLobbyModification() const
	{
		return Handle;
	}
};

USTRUCT(BlueprintType)
struct FEOSLobbyDetailsInfo
{
	GENERATED_BODY()
public:
	/** Lobby id */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** Current owner of the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LobbyOwnerUserId;
	/** Permission level of the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSELobbyPermissionLevel PermissionLevel;
	/** Current available space */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 AvailableSlots;
	/** Max allowed members in the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 MaxMembers;
	/** Are invites allowed */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bAllowInvites;
	/** The main indexed parameter for this lobby, can be any string (ie "Region:GameMode") */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString BucketId;
	/** Is host migration allowed */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bAllowHostMigration;
	/** Was a Real-Time Communication (RTC) room enabled at lobby creation? */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bRTCRoomEnabled;
public:
	explicit FEOSLobbyDetailsInfo() = default;

	FEOSLobbyDetailsInfo(const EOS_LobbyDetails_Info& data)
		: LobbyId(UTF8_TO_TCHAR(data.LobbyId))
		, LobbyOwnerUserId(data.LobbyOwnerUserId)
		, PermissionLevel(static_cast<EEOSELobbyPermissionLevel>(data.PermissionLevel))
		, AvailableSlots(data.AvailableSlots)
		, MaxMembers(data.MaxMembers)
		, bAllowInvites(data.bAllowInvites > 0)
		, BucketId(UTF8_TO_TCHAR(data.BucketId))
		, bAllowHostMigration((data.bAllowHostMigration > 0))
		, bRTCRoomEnabled((data.bRTCRoomEnabled > 0))
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSLobbyLocalRTCOptions
{
	GENERATED_BODY()
public:
	/** Flags for the local user in this room. The default is 0 if this struct is not specified. @see EOS_RTC_JoinRoomOptions::Flags */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 Flags;
	/**
	* Set to EOS_TRUE to enable Manual Audio Input. If manual audio input is enabled, audio recording is not started and the audio buffers
	* must be passed manually using EOS_RTCAudio_SendAudio. The default is EOS_FALSE if this struct is not specified.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bUseManualAudioInput;
	/**
	* Set to EOS_TRUE to enable Manual Audio Output. If manual audio output is enabled, audio rendering is not started and the audio buffers
	* must be received with EOS_RTCAudio_AddNotifyAudioBeforeRender and rendered manually. The default is EOS_FALSE if this struct is not
	* specified.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bUseManualAudioOutput;
	/**
	* Set to EOS_TRUE to start the outgoing audio stream muted by when first connecting to the RTC room. It must be manually unmuted with a
	* call to EOS_RTCAudio_UpdateSending. If manual audio output is enabled, this value is ignored. The default is EOS_FALSE if this struct
	* is not specified.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bAudioOutputStartsMuted;
public:
	explicit FEOSLobbyLocalRTCOptions() = default;

	FEOSLobbyLocalRTCOptions(const EOS_Lobby_LocalRTCOptions& data)
		: Flags(data.Flags)
		, bUseManualAudioInput((data.bUseManualAudioInput > 0))
		, bUseManualAudioOutput((data.bUseManualAudioOutput > 0))
		, bAudioOutputStartsMuted((data.bAudioOutputStartsMuted > 0))
	{}
public:
	operator EOS_Lobby_LocalRTCOptions() const
	{
		EOS_Lobby_LocalRTCOptions Data;
		
		Data.ApiVersion = EOS_LOBBY_LOCALRTCOPTIONS_API_LATEST;
		Data.Flags = Flags;
		Data.bUseManualAudioInput = bUseManualAudioInput;
		Data.bUseManualAudioOutput = bUseManualAudioOutput;
		Data.bAudioOutputStartsMuted = bAudioOutputStartsMuted;

		return Data;
	}
};

/**
 * Input parameters for the EOS_Lobby_CreateLobby Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyCreateLobbyOptions
{
	GENERATED_BODY()
public:
	/** Local user creating the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** Max members allowed in the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 MaxLobbyMembers;
	/** The initial permission level of the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSELobbyPermissionLevel PermissionLevel;
	/** If true, this lobby will be associated with presence information. A user's presence can only be associated with one lobby at a time.
	* This affects the ability of the Social Overlay to show game related actions to take in the user's social graph.
	*
	* @note The Social Overlay can handle only one of the following three options at a time:
	* * using the bPresenceEnabled flags within the Sessions interface
	* * using the bPresenceEnabled flags within the Lobby interface
	* * using EOS_PresenceModification_SetJoinInfo
	*
	* @see EOS_PresenceModification_SetJoinInfoOptions
	* @see EOS_Lobby_JoinLobbyOptions
	* @see EOS_Sessions_CreateSessionModificationOptions
	* @see EOS_Sessions_JoinSessionOptions
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bPresenceEnabled;
	/** Are members of the lobby allowed to invite others */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bAllowInvites;
	/** Bucket ID associated with the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString BucketId;
	/** 
	* Is host migration allowed (will the lobby stay open if the original host leaves?) 
	* NOTE: EOS_Lobby_PromoteMember is still allowed regardless of this setting 
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bDisableHostMigration;
	/**
	* Creates a real-time communication (RTC) room for all members of this lobby. All members of the lobby will automatically join the RTC
	* room when they connect to the lobby and they will automatically leave the RTC room when they leave or are removed from the lobby.
	* While the joining and leaving of the RTC room is automatic, applications will still need to use the EOS RTC interfaces to handle all
	* other functionality for the room.
	*
	* @see EOS_Lobby_GetRTCRoomName
	* @see EOS_Lobby_AddNotifyRTCRoomConnectionChanged
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bEnableRTCRoom;
	/**
	* (Optional) Allows the local application to set local audio options for the RTC Room if it is enabled. Set this to NULL if the RTC
	* RTC room is disabled or you would like to use the defaults.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSLobbyLocalRTCOptions LocalRTCOptions;
	/**
	* (Optional) Set to a globally unique value to override the backend assignment
	* If not specified the backend service will assign one to the lobby.  Do not mix and match override and non override settings.
	* This value can be of size [EOS_LOBBY_MIN_LOBBYIDOVERRIDE_LENGTH, EOS_LOBBY_MAX_LOBBYIDOVERRIDE_LENGTH]
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	explicit FEOSLobbyCreateLobbyOptions()
		: MaxLobbyMembers(10)
		, PermissionLevel(EEOSELobbyPermissionLevel::EOS_LPL_PUBLICADVERTISED)
		, bPresenceEnabled(true)
		, bAllowInvites(true)
		, BucketId("MyBucket")
		, bDisableHostMigration(false)
		, bEnableRTCRoom(false)
	{
	}
};

/**
 * Output parameters for the EOS_Lobby_CreateLobby function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyCreateLobbyCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_CreateLobby */
	void* ClientData;
	/** Newly created lobby id */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
public:
	FEOSLobbyCreateLobbyCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbyCreateLobbyCallbackInfo(const EOS_Lobby_CreateLobbyCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_DestroyLobby Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDestroyLobbyOptions
{
	GENERATED_BODY()
public:
	/** Local user destroying the lobby, must own the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** Lobby Id to destroy */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbyDestroyLobbyOptions() = default;
};

/**
 * Output parameters for the EOS_Lobby_DestroyLobby function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDestroyLobbyCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_DestroyLobby */
	void* ClientData;
	/** Destroyed lobby id */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
public:
	FEOSLobbyDestroyLobbyCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbyDestroyLobbyCallbackInfo(const EOS_Lobby_DestroyLobbyCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_JoinLobby Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyJoinLobbyOptions
{
	GENERATED_BODY()
public:
	/** Lobby handle to join */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSHLobbyDetails LobbyDetailsHandle;
	/** Local user joining the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** If true, this lobby will be associated with the user's presence information. A user can only associate one lobby at a time with their presence information.
	* This affects the ability of the Social Overlay to show game related actions to take in the user's social graph.
	*
	* @note The Social Overlay can handle only one of the following three options at a time:
	* * using the bPresenceEnabled flags within the Sessions interface
	* * using the bPresenceEnabled flags within the Lobby interface
	* * using EOS_PresenceModification_SetJoinInfo
	*
	* @see EOS_PresenceModification_SetJoinInfoOptions
	* @see EOS_Lobby_CreateLobbyOptions
	* @see EOS_Lobby_JoinLobbyOptions
	* @see EOS_Sessions_CreateSessionModificationOptions
	* @see EOS_Sessions_JoinSessionOptions
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bPresenceEnabled;
	/**
	* (Optional) Set this value to override the default local options for the RTC Room, if it is enabled for this lobby. Set this to NULL if
	* your application does not use the Lobby RTC Rooms feature, or if you would like to use the default settings. This option is ignored if
	* the specified lobby does not have an RTC Room enabled and will not cause errors.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSLobbyLocalRTCOptions LocalRTCOptions;
public:
	FEOSLobbyJoinLobbyOptions() = default;
};

/**
 * Output parameters for the EOS_Lobby_JoinLobby function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyJoinLobbyCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_JoinLobby */
	void* ClientData;
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
public:
	FEOSLobbyJoinLobbyCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbyJoinLobbyCallbackInfo(const EOS_Lobby_JoinLobbyCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_LeaveLobby Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyLeaveLobbyOptions
{
	GENERATED_BODY()
public:
	/** Local user leaving the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbyLeaveLobbyOptions() = default;
};

/**
 * Output parameters for the EOS_Lobby_LeaveLobby function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyLeaveLobbyCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_LeaveLobby */
	void* ClientData;
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
public:
	FEOSLobbyLeaveLobbyCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbyLeaveLobbyCallbackInfo(const EOS_Lobby_LeaveLobbyCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_UpdateLobbyModification Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyUpdateLobbyModificationOptions
{
	GENERATED_BODY()
public:
	/** The id of the local user making modifications, must be the owner to modify lobby data, but may be a lobby member to modify their own attributes */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbyUpdateLobbyModificationOptions() = default;
};

/**
 * Input parameters for the EOS_Lobby_UpdateLobby Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyUpdateLobbyOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Builder handle */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSHLobbyModification LobbyModificationHandle;
public:
public:
	FEOSLobbyUpdateLobbyOptions()
		: ApiVersion(EOS_LOBBY_UPDATELOBBY_API_LATEST)
	{
	}

	FEOSLobbyUpdateLobbyOptions(const EOS_Lobby_UpdateLobbyOptions& data)
		: ApiVersion(EOS_LOBBY_UPDATELOBBY_API_LATEST)
		, LobbyModificationHandle(data.LobbyModificationHandle)
	{
	}
};

/**
 * Output parameters for the EOS_Lobby_UpdateLobby function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyUpdateLobbyCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_UpdateLobby */
	void* ClientData;
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbyUpdateLobbyCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbyUpdateLobbyCallbackInfo(const EOS_Lobby_UpdateLobbyCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_PromoteMember Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyPromoteMemberOptions
{
	GENERATED_BODY()
public:
	/** Lobby id of interest */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** Local User making the request */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** Member to promote to owner of the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
public:
	FEOSLobbyPromoteMemberOptions() = default;
};

/**
 * Output parameters for the EOS_Lobby_PromoteMember function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyPromoteMemberCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_PromoteMember */
	void* ClientData;
	/** Lobby id of interest */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbyPromoteMemberCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbyPromoteMemberCallbackInfo(const EOS_Lobby_PromoteMemberCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_KickMember Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyKickMemberOptions
{
	GENERATED_BODY()
public:
	/** Lobby id of interest */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** Local User making the request */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** Member to kick from the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
public:
	FEOSLobbyKickMemberOptions() = default;
};

/**
 * Output parameters for the EOS_Lobby_KickMember function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyKickMemberCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_KickMember */
	void* ClientData;
	/** Lobby id of interest */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbyKickMemberCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbyKickMemberCallbackInfo(const EOS_Lobby_KickMemberCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSLobbyAddNotifyLobbyUpdateReceivedOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSNotificationId NotificationID;
public:
	FEOSLobbyAddNotifyLobbyUpdateReceivedOptions()
		: ApiVersion(EOS_LOBBY_ADDNOTIFYLOBBYUPDATERECEIVED_API_LATEST)
	{
	}

	FEOSLobbyAddNotifyLobbyUpdateReceivedOptions(const EOS_Lobby_AddNotifyLobbyUpdateReceivedOptions& data)
		: ApiVersion(EOS_LOBBY_ADDNOTIFYLOBBYUPDATERECEIVED_API_LATEST)
	{
	}
};

/**
 * Output parameters for the EOS_Lobby_OnLobbyUpdateReceivedCallback Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyUpdateReceivedCallbackInfo
{
	GENERATED_BODY()
public:
	/** Context that was passed into EOS_Lobby_KickMember */
	void* ClientData;
	/** Lobby id of interest */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbyUpdateReceivedCallbackInfo()
		: ClientData(nullptr)
	{
	}

	FEOSLobbyUpdateReceivedCallbackInfo(const EOS_Lobby_LobbyUpdateReceivedCallbackInfo& data)
		: ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSLobbyAddNotifyLobbyMemberUpdateReceivedOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSNotificationId NotificationID;
public:
	FEOSLobbyAddNotifyLobbyMemberUpdateReceivedOptions()
		: ApiVersion(EOS_LOBBY_ADDNOTIFYLOBBYMEMBERUPDATERECEIVED_API_LATEST)
	{
	}
};

/**
 * Output parameters for the EOS_Lobby_OnLobbyMemberUpdateReceivedCallback Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyMemberUpdateReceivedCallbackInfo
{
	GENERATED_BODY()
public:
	/** Context that was passed into EOS_Lobby_AddNotifyLobbyMemberUpdateReceived */
	void* ClientData;
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** Target user that was affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
public:
	FEOSLobbyMemberUpdateReceivedCallbackInfo()
		: ClientData(nullptr)
	{
	}

	FEOSLobbyMemberUpdateReceivedCallbackInfo(const EOS_Lobby_LobbyMemberUpdateReceivedCallbackInfo& data)
		: ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
		, TargetUserId(data.TargetUserId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_AddNotifyLobbyMemberStatusReceived Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyAddNotifyLobbyMemberStatusReceivedOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSNotificationId NotificationID;
public:
	FEOSLobbyAddNotifyLobbyMemberStatusReceivedOptions()
		: ApiVersion(EOS_LOBBY_ADDNOTIFYLOBBYMEMBERSTATUSRECEIVED_API_LATEST)
	{
	}

	FEOSLobbyAddNotifyLobbyMemberStatusReceivedOptions(const EOS_Lobby_AddNotifyLobbyMemberStatusReceivedOptions& data)
		: ApiVersion(EOS_LOBBY_ADDNOTIFYLOBBYMEMBERSTATUSRECEIVED_API_LATEST)
	{
	}
};

/**
 * Output parameters for the EOS_Lobby_AddNotifyLobbyMemberStatusReceived function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyMemberStatusReceivedCallbackInfo
{
	GENERATED_BODY()
public:
	/** Context that was passed into EOS_Lobby_AddNotifyLobbyMemberStatusReceived */
	void* ClientData;
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** Target user that was affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
	/** Latest status of the user */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSELobbyMemberStatus CurrentStatus;
public:
	FEOSLobbyMemberStatusReceivedCallbackInfo()
		: ClientData(nullptr)
		, CurrentStatus(EEOSELobbyMemberStatus::EOS_LMS_CLOSED)
	{
	}

	FEOSLobbyMemberStatusReceivedCallbackInfo(const EOS_Lobby_LobbyMemberStatusReceivedCallbackInfo& data)
		: ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
		, TargetUserId(data.TargetUserId)
		, CurrentStatus(static_cast<EEOSELobbyMemberStatus>(data.CurrentStatus))
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSLobbyAddNotifyLobbyInviteReceivedOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSNotificationId NotificationID;
public:
	FEOSLobbyAddNotifyLobbyInviteReceivedOptions()
		: ApiVersion(EOS_LOBBY_ADDNOTIFYLOBBYINVITERECEIVED_API_LATEST)
	{
	}

	FEOSLobbyAddNotifyLobbyInviteReceivedOptions(const EOS_Lobby_AddNotifyLobbyInviteReceivedOptions& data)
		: ApiVersion(EOS_LOBBY_ADDNOTIFYLOBBYINVITERECEIVED_API_LATEST)
	{
	}
};

/**
 * Output parameters for the EOS_Lobby_OnLobbyInviteReceivedCallback Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyInviteReceivedCallbackInfo
{
	GENERATED_BODY()
public:
	/** Context that was passed into EOS_Lobby_AddNotifyLobbyInviteReceived */
	void* ClientData;
	/** The invite id */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString InviteId;
	/** User that received the invite */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** Target user that sent the invite */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
public:
	FEOSLobbyInviteReceivedCallbackInfo()
		: ClientData(nullptr)
	{
	}

	FEOSLobbyInviteReceivedCallbackInfo(const EOS_Lobby_LobbyInviteReceivedCallbackInfo& data)
		: ClientData(data.ClientData)
		, InviteId(UTF8_TO_TCHAR(data.InviteId))
		, LocalUserId(data.LocalUserId)
		, TargetUserId(data.TargetUserId)
	{
	}
};

/** The most recent version of the EOS_Lobby_AddNotifyLobbyInviteAccepted API. */
USTRUCT(BlueprintType)
struct FEOSLobbyAddNotifyLobbyInviteAcceptedOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSLobbyAddNotifyLobbyInviteAcceptedOptions()
		: ApiVersion(EOS_LOBBY_ADDNOTIFYLOBBYINVITEACCEPTED_API_LATEST)
	{
	}
};

/**
 * Output parameters for the EOS_Lobby_OnLobbyInviteAcceptedCallback Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyInviteAcceptedCallbackInfo
{
	GENERATED_BODY()
public:
	/** Context that was passed into EOS_Lobby_AddNotifyLobbyInviteReceived */
	void* ClientData;
	/** The invite id */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString InviteId;
	/** User that received the invite */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** Target user that sent the invite */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
	/** Lobby ID that the user has been invited to */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbyInviteAcceptedCallbackInfo()
		: ClientData(nullptr)
	{
	}

	FEOSLobbyInviteAcceptedCallbackInfo(const EOS_Lobby_LobbyInviteAcceptedCallbackInfo& data)
		: ClientData(data.ClientData)
		, InviteId(data.InviteId)
		, LocalUserId(data.LocalUserId)
		, TargetUserId(data.TargetUserId)
		, LobbyId(UTF8_TO_TCHAR(data.LobbyId))
	{
	}
};

/**
* Output parameters for the EOS_Lobby_OnJoinLobbyAcceptedCallback Function.
*/
USTRUCT(BlueprintType)
struct FEOSLobbyJoinLobbyAcceptedCallbackInfo
{
	GENERATED_BODY()
public:
	/** Context that was passed into EOS_Lobby_AddNotifyJoinLobbyAccepted */
	void* ClientData;
	/** The Product User ID of the local user who is joining */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** 
	* The UI Event associated with this Join Game event.
	* This should be used with EOS_Lobby_CopyLobbyDetailsHandleByUiEventId to get a handle to be used
	* when calling EOS_Lobby_JoinLobby.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSUIEventId UiEventId;
public:
	FEOSLobbyJoinLobbyAcceptedCallbackInfo()
		: ClientData(nullptr)
	{
	}

	FEOSLobbyJoinLobbyAcceptedCallbackInfo(const EOS_Lobby_JoinLobbyAcceptedCallbackInfo& data)
		: ClientData(data.ClientData)
		, LocalUserId(data.LocalUserId)
		, UiEventId(data.UiEventId)
	{
	}
};

/** The most recent version of the EOS_Lobby_AddNotifyJoinGameAccepted API. */
USTRUCT(BlueprintType)
struct FEOSLobbyAddNotifyJoinLobbyAcceptedOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSLobbyAddNotifyJoinLobbyAcceptedOptions()
		: ApiVersion(EOS_LOBBY_ADDNOTIFYJOINLOBBYACCEPTED_API_LATEST)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_CopyLobbyDetailsHandleByInviteId Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyCopyLobbyDetailsHandleByInviteIdOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Lobby invite id */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString InviteId;
public:
	FEOSLobbyCopyLobbyDetailsHandleByInviteIdOptions()
		: ApiVersion(EOS_LOBBY_COPYLOBBYDETAILSHANDLEBYINVITEID_API_LATEST)
	{
	}
};

/**
* Input parameters for the EOS_Lobby_GetRTCRoomName function.
*/
USTRUCT(BlueprintType)
struct FEOSLobbyGetRTCRoomNameOptions
{
	GENERATED_BODY()
public:
	/** The ID of the lobby to get the RTC Room name for */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** The Product User ID of the local user in the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
public:
	FEOSLobbyGetRTCRoomNameOptions() = default;
};

USTRUCT(BlueprintType)
struct FEOSLobbyIsRTCRoomConnectedOptions
{
	GENERATED_BODY()
public:
	/** The ID of the lobby to get the RTC Room name for */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** The Product User ID of the local user in the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
public:
	FEOSLobbyIsRTCRoomConnectedOptions() = default;
};

/**
* Input parameters for the EOS_Lobby_AddNotifyRTCRoomConnectionChanged function.
*/
USTRUCT(BlueprintType)
struct FEOSLobbyAddNotifyRTCRoomConnectionChangedOptions
{
	GENERATED_BODY()
public:
	/** The ID of the lobby to receive RTC Room connection change notifications for */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** The Product User ID of the local user in the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
public:
	FEOSLobbyAddNotifyRTCRoomConnectionChangedOptions() = default;
};

USTRUCT(BlueprintType)
struct FEOSLobbyRTCRoomConnectionChangedCallbackInfo
{
	GENERATED_BODY()
public:
	/** Context that was passed into EOS_Lobby_AddNotifyRTCRoomConnectionChanged */
	void* ClientData;
	/** The ID of the lobby which had a RTC Room connection state change */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** The Product User ID of the local user who is in the lobby and registered for notifications */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The new connection state of the room */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bIsConnected;
	/**
	If bIsConnected is EOS_FALSE, this result will be the reason we were disconnected.
	* EOS_Success: The room was left locally. This may be because: the associated lobby was Left or Destroyed, the connection to the lobby was interrupted, or because the SDK is being shutdown. If the lobby connection returns (lobby did not permanently go away), we will reconnect.
	* EOS_NoConnection: There was a network issue connecting to the server. We will attempt to reconnect soon.
	* EOS_RTC_UserKicked: The user has been kicked by the server. We will not reconnect.
	* EOS_RTC_UserBanned: The user has been banned by the server. We will not reconnect.
	* EOS_ServiceFailure: A known error occurred during interaction with the server. We will attempt to reconnect soon.
	* EOS_UnexpectedError: Unexpected error. We will attempt to reconnect soon.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult DisconnectReason;
public:
	FEOSLobbyRTCRoomConnectionChangedCallbackInfo() = default;

	FEOSLobbyRTCRoomConnectionChangedCallbackInfo(const EOS_Lobby_RTCRoomConnectionChangedCallbackInfo& data)
		: ClientData(data.ClientData)
		, LobbyId(UTF8_TO_TCHAR(data.LobbyId))
		, LocalUserId(data.LocalUserId)
		, bIsConnected(data.bIsConnected > 0)
		, DisconnectReason(EOSHelpers::ToEOSCoreResult(data.DisconnectReason))
	{
	}
};


/**
 * Input parameters for the EOS_Lobby_CreateLobbySearch Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyCreateLobbySearchOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Maximum number of results allowed from the search */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 MaxResults;
public:
	explicit FEOSLobbyCreateLobbySearchOptions()
		: ApiVersion(EOS_LOBBY_CREATELOBBYSEARCH_API_LATEST)
		, MaxResults(50)
	{
	}

	FEOSLobbyCreateLobbySearchOptions(const EOS_Lobby_CreateLobbySearchOptions& data)
		: ApiVersion(EOS_LOBBY_CREATELOBBYSEARCH_API_LATEST)
		, MaxResults(data.MaxResults)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_SendInvite Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySendInviteOptions
{
	GENERATED_BODY()
public:
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** Local user sending the invite */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** Target user receiving the invite */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
public:
	FEOSLobbySendInviteOptions() = default;
};

/**
 * Output parameters for the EOS_Lobby_SendInvite function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySendInviteCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_SendInvite */
	void* ClientData;
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbySendInviteCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbySendInviteCallbackInfo(const EOS_Lobby_SendInviteCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LobbyId(data.LobbyId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_RejectInvite Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyRejectInviteOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** The invite id to reject */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString InviteId;
	/** Local user rejecting the invite */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
public:
	FEOSLobbyRejectInviteOptions()
		: ApiVersion(EOS_LOBBY_REJECTINVITE_API_LATEST)
	{
	}
};

/**
 * Output parameters for the EOS_Lobby_RejectInvite function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyRejectInviteCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_RejectInvite */
	void* ClientData;
	/** The invite id to reject */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString InviteId;
public:
	FEOSLobbyRejectInviteCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbyRejectInviteCallbackInfo(const EOS_Lobby_RejectInviteCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, InviteId(UTF8_TO_TCHAR(data.InviteId))
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_QueryInvites Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyQueryInvitesOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Local User Id to query invites */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
public:
	FEOSLobbyQueryInvitesOptions()
		: ApiVersion(EOS_LOBBY_QUERYINVITES_API_LATEST)
	{
	}

	FEOSLobbyQueryInvitesOptions(const EOS_Lobby_QueryInvitesOptions& data)
		: ApiVersion(EOS_LOBBY_QUERYINVITES_API_LATEST)
		, LocalUserId(data.LocalUserId)
	{
	}
};

/**
 * Output parameters for the EOS_Lobby_QueryInvites function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyQueryInvitesCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_Lobby_QueryInvites */
	void* ClientData;
	/** Local User Id that made the request */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
public:
	FEOSLobbyQueryInvitesCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbyQueryInvitesCallbackInfo(const EOS_Lobby_QueryInvitesCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LocalUserId(data.LocalUserId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_GetInviteCount Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyGetInviteCountOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Local user that has invites */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
public:
	FEOSLobbyGetInviteCountOptions()
		: ApiVersion(EOS_LOBBY_GETINVITECOUNT_API_LATEST)
	{
	}

	FEOSLobbyGetInviteCountOptions(const EOS_Lobby_GetInviteCountOptions& data)
		: ApiVersion(EOS_LOBBY_GETINVITECOUNT_API_LATEST)
		, LocalUserId(data.LocalUserId)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_GetInviteIdByIndex Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyGetInviteIdByIndexOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Local user that has invites */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** Index of the invite id to retrieve */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 Index;
public:
	explicit FEOSLobbyGetInviteIdByIndexOptions()
		: ApiVersion(EOS_LOBBY_GETINVITEIDBYINDEX_API_LATEST)
		, Index(0)
	{
	}

	FEOSLobbyGetInviteIdByIndexOptions(const EOS_Lobby_GetInviteIdByIndexOptions& data)
		: ApiVersion(EOS_LOBBY_GETINVITEIDBYINDEX_API_LATEST)
		, LocalUserId(data.LocalUserId)
		, Index(data.Index)
	{
	}
};

/**
 * Input parameters for the EOS_Lobby_CopyLobbyDetailsHandle Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyCopyLobbyDetailsHandleOptions
{
	GENERATED_BODY()
public:
	/** The id of the lobby affected */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
	/** Local user making the request */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
public:
	FEOSLobbyCopyLobbyDetailsHandleOptions() = default;
};

/**
* Input parameters for the EOS_LobbyModification_SetBucketIdOptions function.
*/
USTRUCT(BlueprintType)
struct FEOSLobbyModificationSetBucketIdOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** The new bucket id associated with the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString BucketId;
public:
	FEOSLobbyModificationSetBucketIdOptions()
		: ApiVersion(EOS_LOBBYMODIFICATION_SETBUCKETID_API_LATEST)
	{
	}

	FEOSLobbyModificationSetBucketIdOptions(const EOS_LobbyModification_SetBucketIdOptions& data)
		: ApiVersion(EOS_LOBBYMODIFICATION_SETBUCKETID_API_LATEST)
		, BucketId(UTF8_TO_TCHAR(data.BucketId))
	{
	}
};

/**
 * Contains information about lobby and lobby member data
 */
USTRUCT(BlueprintType)
struct FEOSLobbyAttributeData
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
	/** Contains information about lobby and lobby member data */
	EOS_Lobby_AttributeData AttributeData;
public:
	FEOSLobbyAttributeData()
		: ApiVersion(EOS_LOBBY_ATTRIBUTEDATA_API_LATEST)
	{
	}

	FEOSLobbyAttributeData(const EOS_Lobby_AttributeData& data)
		: ApiVersion(EOS_LOBBY_ATTRIBUTEDATA_API_LATEST)
		, AttributeData(data)
	{
	}
};

/**
 *  An attribute and its visibility setting stored with a lobby.
 *  Used to store both lobby and lobby member data
 */
USTRUCT(BlueprintType)
struct FEOSLobbyAttribute
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Key/Value pair describing the attribute */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSLobbyAttributeData Data;
	/** Is this attribute public or private to the lobby and its members */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSELobbyAttributeVisibility Visbility;
public:
	explicit FEOSLobbyAttribute()
		: ApiVersion(EOS_LOBBY_ATTRIBUTE_API_LATEST)
		, Visbility(EEOSELobbyAttributeVisibility::EOS_LAT_PUBLIC)
	{
	}

	FEOSLobbyAttribute(const EOS_Lobby_Attribute& data)
		: ApiVersion(EOS_LOBBY_ATTRIBUTE_API_LATEST)
		, Data(*data.Data)
		, Visbility(static_cast<EEOSELobbyAttributeVisibility>(data.Visibility))
	{
	}
};

/**
 * Input parameters for the EOS_LobbyModification_SetPermissionLevel Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyModificationSetPermissionLevelOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Permission level of the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSELobbyPermissionLevel PermissionLevel;
public:
	explicit FEOSLobbyModificationSetPermissionLevelOptions()
		: ApiVersion(EOS_LOBBYMODIFICATION_SETPERMISSIONLEVEL_API_LATEST)
		, PermissionLevel(EEOSELobbyPermissionLevel::EOS_LPL_PUBLICADVERTISED)
	{
	}

	FEOSLobbyModificationSetPermissionLevelOptions(const EOS_LobbyModification_SetPermissionLevelOptions& data)
		: ApiVersion(EOS_LOBBYMODIFICATION_SETPERMISSIONLEVEL_API_LATEST)
		, PermissionLevel(static_cast<EEOSELobbyPermissionLevel>(data.PermissionLevel))
	{
	}
};

/**
 * Input parameters for the EOS_LobbyModification_SetMaxMembers Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyModificationSetMaxMembersOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** New maximum number of lobby members */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 MaxMembers;
public:
	explicit FEOSLobbyModificationSetMaxMembersOptions()
		: ApiVersion(EOS_LOBBYMODIFICATION_SETMAXMEMBERS_API_LATEST)
		, MaxMembers(10)
	{
	}

	FEOSLobbyModificationSetMaxMembersOptions(const EOS_LobbyModification_SetMaxMembersOptions& data)
		: ApiVersion(EOS_LOBBYMODIFICATION_SETMAXMEMBERS_API_LATEST)
		, MaxMembers(data.MaxMembers)
	{
	}
};

/**
* Input parameters for the EOS_LobbyModification_SetInvitesAllowed Function.
*/
USTRUCT(BlueprintType)
struct FEOSLobbyModificationSetInvitesAllowedOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** If true then invites can currently be sent for the associated lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bInvitesAllowed;
public:
	explicit FEOSLobbyModificationSetInvitesAllowedOptions()
		: ApiVersion(EOS_LOBBYMODIFICATION_SETINVITESALLOWED_API_LATEST)
		, bInvitesAllowed(true)
	{
	}

	FEOSLobbyModificationSetInvitesAllowedOptions(const EOS_LobbyModification_SetInvitesAllowedOptions& data)
		: ApiVersion(EOS_LOBBYMODIFICATION_SETINVITESALLOWED_API_LATEST)
		, bInvitesAllowed(data.bInvitesAllowed > 0)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyModification_AddAttribute Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyModificationAddAttributeOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Key/Value pair describing the attribute to add to the lobby */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSLobbyAttributeData Attribute;
	/** Is this attribute public or private to the lobby and its members */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSELobbyAttributeVisibility Visibility;
public:
	explicit FEOSLobbyModificationAddAttributeOptions()
		: ApiVersion(EOS_LOBBYMODIFICATION_ADDATTRIBUTE_API_LATEST)
		, Visibility(EEOSELobbyAttributeVisibility::EOS_LAT_PUBLIC)
	{
	}

	FEOSLobbyModificationAddAttributeOptions(const EOS_LobbyModification_AddAttributeOptions& data)
		: ApiVersion(EOS_LOBBYMODIFICATION_ADDATTRIBUTE_API_LATEST)
		, Attribute(*data.Attribute)
		, Visibility(static_cast<EEOSELobbyAttributeVisibility>(data.Visibility))
	{
	}
};

/**
 * Input parameters for the EOS_LobbyModification_RemoveAttribute Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyModificationRemoveAttributeOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Name of the key */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString Key;
public:
	FEOSLobbyModificationRemoveAttributeOptions()
		: ApiVersion(EOS_LOBBYMODIFICATION_REMOVEATTRIBUTE_API_LATEST)
	{
	}

	FEOSLobbyModificationRemoveAttributeOptions(const EOS_LobbyModification_RemoveAttributeOptions& data)
		: ApiVersion(EOS_LOBBYMODIFICATION_REMOVEATTRIBUTE_API_LATEST)
		, Key(data.Key)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyModification_AddMemberAttribute Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyModificationAddMemberAttributeOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Key/Value pair describing the attribute to add to the lobby member */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSLobbyAttributeData Attribute;
	/** Is this attribute public or private to the rest of the lobby members */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSELobbyAttributeVisibility Visibility;
public:
	explicit FEOSLobbyModificationAddMemberAttributeOptions()
		: ApiVersion(EOS_LOBBYMODIFICATION_ADDMEMBERATTRIBUTE_API_LATEST)
		, Visibility(EEOSELobbyAttributeVisibility::EOS_LAT_PUBLIC)
	{
	}

	FEOSLobbyModificationAddMemberAttributeOptions(const EOS_LobbyModification_AddMemberAttributeOptions& data)
		: ApiVersion(EOS_LOBBYMODIFICATION_ADDMEMBERATTRIBUTE_API_LATEST)
		, Attribute(*data.Attribute)
		, Visibility(static_cast<EEOSELobbyAttributeVisibility>(data.Visibility))
	{
	}
};

/**
 * Input parameters for the EOS_LobbyModification_RemoveMemberAttribute Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyModificationRemoveMemberAttributeOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Name of the key */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString Key;
public:
	FEOSLobbyModificationRemoveMemberAttributeOptions()
		: ApiVersion(EOS_LOBBYMODIFICATION_REMOVEMEMBERATTRIBUTE_API_LATEST)
	{
	}

	FEOSLobbyModificationRemoveMemberAttributeOptions(const EOS_LobbyModification_RemoveMemberAttributeOptions& data)
		: ApiVersion(EOS_LOBBYMODIFICATION_REMOVEMEMBERATTRIBUTE_API_LATEST)
		, Key(data.Key)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_GetLobbyOwner Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsGetLobbyOwnerOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSLobbyDetailsGetLobbyOwnerOptions()
		: ApiVersion(EOS_LOBBYDETAILS_GETLOBBYOWNER_API_LATEST)
	{
	}

	FEOSLobbyDetailsGetLobbyOwnerOptions(const EOS_LobbyDetails_GetLobbyOwnerOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_GETLOBBYOWNER_API_LATEST)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_CopyInfo Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsCopyInfoOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSLobbyDetailsCopyInfoOptions()
		: ApiVersion(EOS_LOBBYDETAILS_COPYINFO_API_LATEST)
	{
	}

	FEOSLobbyDetailsCopyInfoOptions(const EOS_LobbyDetails_CopyInfoOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_COPYINFO_API_LATEST)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_GetAttributeCount Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsGetAttributeCountOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSLobbyDetailsGetAttributeCountOptions()
		: ApiVersion(EOS_LOBBYDETAILS_GETATTRIBUTECOUNT_API_LATEST)
	{
	}

	FEOSLobbyDetailsGetAttributeCountOptions(const EOS_LobbyDetails_GetAttributeCountOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_GETATTRIBUTECOUNT_API_LATEST)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_CopyAttributeByIndex Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsCopyAttributeByIndexOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/**
	* The index of the attribute to retrieve
	* @see EOS_LobbyDetails_GetAttributeCount
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 AttrIndex;
public:
	explicit FEOSLobbyDetailsCopyAttributeByIndexOptions()
		: ApiVersion(EOS_LOBBYDETAILS_COPYATTRIBUTEBYINDEX_API_LATEST)
		, AttrIndex(0)
	{
	}

	FEOSLobbyDetailsCopyAttributeByIndexOptions(const EOS_LobbyDetails_CopyAttributeByIndexOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_COPYATTRIBUTEBYINDEX_API_LATEST)
		, AttrIndex(data.AttrIndex)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_CopyAttributeByKey Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsCopyAttributeByKeyOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Name of the attribute */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString AttrKey;
public:
	FEOSLobbyDetailsCopyAttributeByKeyOptions()
		: ApiVersion(EOS_LOBBYDETAILS_COPYATTRIBUTEBYINDEX_API_LATEST)
	{
	}

	FEOSLobbyDetailsCopyAttributeByKeyOptions(const EOS_LobbyDetails_CopyAttributeByKeyOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_COPYATTRIBUTEBYINDEX_API_LATEST)
		, AttrKey(data.AttrKey)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_GetMemberAttributeCount Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsGetMemberAttributeCountOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Lobby member of interest */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
public:
	FEOSLobbyDetailsGetMemberAttributeCountOptions()
		: ApiVersion(EOS_LOBBYDETAILS_GETMEMBERATTRIBUTECOUNT_API_LATEST)
	{
	}

	FEOSLobbyDetailsGetMemberAttributeCountOptions(const EOS_LobbyDetails_GetMemberAttributeCountOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_GETMEMBERATTRIBUTECOUNT_API_LATEST)
		, TargetUserId(data.TargetUserId)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_CopyMemberAttributeByIndex Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsCopyMemberAttributeByIndexOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Lobby member of interest */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
	/** Attribute index */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 AttrIndex;
public:
	explicit FEOSLobbyDetailsCopyMemberAttributeByIndexOptions()
		: ApiVersion(EOS_LOBBYDETAILS_COPYMEMBERATTRIBUTEBYINDEX_API_LATEST)
		, AttrIndex(0)
	{
	}

	FEOSLobbyDetailsCopyMemberAttributeByIndexOptions(const EOS_LobbyDetails_CopyMemberAttributeByIndexOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_COPYMEMBERATTRIBUTEBYINDEX_API_LATEST)
		, TargetUserId(data.TargetUserId)
		, AttrIndex(data.AttrIndex)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_CopyMemberAttributeByKey Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsCopyMemberAttributeByKeyOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Lobby member of interest */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
	/** Name of the attribute */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString AttrKey;
public:
	FEOSLobbyDetailsCopyMemberAttributeByKeyOptions()
		: ApiVersion(EOS_LOBBYDETAILS_COPYMEMBERATTRIBUTEBYKEY_API_LATEST)
	{
	}

	FEOSLobbyDetailsCopyMemberAttributeByKeyOptions(const EOS_LobbyDetails_CopyMemberAttributeByKeyOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_COPYMEMBERATTRIBUTEBYKEY_API_LATEST)
		, TargetUserId(data.TargetUserId)
		, AttrKey(data.AttrKey)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_GetMemberCount Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsGetMemberCountOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSLobbyDetailsGetMemberCountOptions()
		: ApiVersion(EOS_LOBBYDETAILS_GETMEMBERCOUNT_API_LATEST)
	{
	}

	FEOSLobbyDetailsGetMemberCountOptions(const EOS_LobbyDetails_GetMemberCountOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_GETMEMBERCOUNT_API_LATEST)
	{
	}
};

/**
 * Input parameters for the EOS_LobbyDetails_GetMemberByIndex Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbyDetailsGetMemberByIndexOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Index of the member to retrieve */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 MemberIndex;
public:
	explicit FEOSLobbyDetailsGetMemberByIndexOptions()
		: ApiVersion(EOS_LOBBYDETAILS_GETMEMBERBYINDEX_API_LATEST)
		, MemberIndex(0)
	{
	}

	FEOSLobbyDetailsGetMemberByIndexOptions(const EOS_LobbyDetails_GetMemberByIndexOptions& data)
		: ApiVersion(EOS_LOBBYDETAILS_GETMEMBERBYINDEX_API_LATEST)
		, MemberIndex(data.MemberIndex)
	{
	}
};

/**
 * Input parameters for the EOS_LobbySearch_Find Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySearchFindOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** User making the search request */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
public:
	FEOSLobbySearchFindOptions()
		: ApiVersion(EOS_LOBBYSEARCH_FIND_API_LATEST)
	{
	}

	FEOSLobbySearchFindOptions(const EOS_LobbySearch_FindOptions& data)
		: ApiVersion(EOS_LOBBYSEARCH_FIND_API_LATEST)
		, LocalUserId(data.LocalUserId)
	{
	}
};

/**
 * Output parameters for the EOS_LobbySearch_Find function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySearchFindCallbackInfo
{
	GENERATED_BODY()
public:
	/** Result code for the operation. EOS_Success is returned for a successful operation, otherwise one of the error codes is returned. See eos_common.h */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Context that was passed into EOS_LobbySearch_Find */
	void* ClientData;
public:
	FEOSLobbySearchFindCallbackInfo()
		: ResultCode(EOSResult::EOS_ServiceFailure)
		, ClientData(nullptr)
	{
	}

	FEOSLobbySearchFindCallbackInfo(const EOS_LobbySearch_FindCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
	{
	}
};

/**
 * Input parameters for the EOS_LobbySearch_SetLobbyId Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySearchSetLobbyIdOptions
{
	GENERATED_BODY()
public:
	/** The id of the lobby to set */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString LobbyId;
public:
	FEOSLobbySearchSetLobbyIdOptions() = default;
};

/**
 * Input parameters for the EOS_LobbySearch_SetTargetUserId Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySearchSetTargetUserIdOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Search lobbies for given user, returning any lobbies where this user is currently registered */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId TargetUserId;
public:
	FEOSLobbySearchSetTargetUserIdOptions()
		: ApiVersion(EOS_LOBBYSEARCH_SETTARGETUSERID_API_LATEST)
	{
	}

	FEOSLobbySearchSetTargetUserIdOptions(const EOS_LobbySearch_SetTargetUserIdOptions& data)
		: ApiVersion(EOS_LOBBYSEARCH_SETTARGETUSERID_API_LATEST)
		, TargetUserId(data.TargetUserId)
	{
	}
};

/**
 * Input parameters for the EOS_LobbySearch_SetParameter Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySearchSetParameterOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Search parameter describing a key and a value to compare */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSLobbyAttributeData Parameter;
	/** The type of comparison to make against the search parameter */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSEComparisonOp ComparisonOp;
public:
	explicit FEOSLobbySearchSetParameterOptions()
		: ApiVersion(EOS_LOBBYSEARCH_SETPARAMETER_API_LATEST)
		, ComparisonOp(EEOSEComparisonOp::EOS_CO_EQUAL)
	{
	}

	FEOSLobbySearchSetParameterOptions(const EOS_LobbySearch_SetParameterOptions& data)
		: ApiVersion(EOS_LOBBYSEARCH_SETPARAMETER_API_LATEST)
		, Parameter(*data.Parameter)
		, ComparisonOp(static_cast<EEOSEComparisonOp>(data.ComparisonOp))
	{
	}
};

/**
 * Input parameters for the EOS_LobbySearch_RemoveParameter Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySearchRemoveParameterOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Search parameter key to remove from the search */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString Key;
	/** Search comparison operation associated with the key to remove */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSEComparisonOp ComparisonOp;
public:
	FEOSLobbySearchRemoveParameterOptions()
		: ApiVersion(EOS_LOBBYSEARCH_REMOVEPARAMETER_API_LATEST)
		, ComparisonOp(EEOSEComparisonOp::EOS_CO_EQUAL)
	{
	}

	FEOSLobbySearchRemoveParameterOptions(const EOS_LobbySearch_RemoveParameterOptions& data)
		: ApiVersion(EOS_LOBBYSEARCH_REMOVEPARAMETER_API_LATEST)
		, Key(data.Key)
		, ComparisonOp(static_cast<EEOSEComparisonOp>(data.ComparisonOp))
	{
	}
};

/**
 * Input parameters for the EOS_LobbySearch_SetMaxResults Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySearchSetMaxResultsOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/** Maximum number of search results to return from the query */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 MaxResults;
public:
	FEOSLobbySearchSetMaxResultsOptions()
		: ApiVersion(EOS_LOBBYSEARCH_SETMAXRESULTS_API_LATEST)
		, MaxResults(50)
	{
	}

	FEOSLobbySearchSetMaxResultsOptions(const EOS_LobbySearch_SetMaxResultsOptions& data)
		: ApiVersion(EOS_LOBBYSEARCH_SETMAXRESULTS_API_LATEST)
		, MaxResults(data.MaxResults)
	{
	}
};

/**
 * Input parameters for the EOS_LobbySearch_GetSearchResultCount Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySearchGetSearchResultCountOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	FEOSLobbySearchGetSearchResultCountOptions()
		: ApiVersion(EOS_LOBBYSEARCH_GETSEARCHRESULTCOUNT_API_LATEST)
	{
	}

	FEOSLobbySearchGetSearchResultCountOptions(const EOS_LobbySearch_GetSearchResultCountOptions& data)
		: ApiVersion(EOS_LOBBYSEARCH_GETSEARCHRESULTCOUNT_API_LATEST)
	{
	}
};

/**
 * Input parameters for the EOS_LobbySearch_CopySearchResultByIndex Function.
 */
USTRUCT(BlueprintType)
struct FEOSLobbySearchCopySearchResultByIndexOptions
{
	GENERATED_BODY()
public:
	/** API Version */
	int32 ApiVersion;
public:
	/**
	* The index of the lobby to retrieve within the completed search query
	 * @see EOS_LobbySearch_GetSearchResultCount
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 LobbyIndex;
public:
	explicit FEOSLobbySearchCopySearchResultByIndexOptions()
		: ApiVersion(EOS_LOBBYSEARCH_COPYSEARCHRESULTBYINDEX_API_LATEST)
		, LobbyIndex(0)
	{
	}

	FEOSLobbySearchCopySearchResultByIndexOptions(const EOS_LobbySearch_CopySearchResultByIndexOptions& data)
		: ApiVersion(EOS_LOBBYSEARCH_COPYSEARCHRESULTBYINDEX_API_LATEST)
		, LobbyIndex(data.LobbyIndex)
	{
	}
};

/** Handle to the calls responsible for creating a search object */
USTRUCT(BlueprintType)
struct FEOSHLobbySearch
{
	GENERATED_BODY()
public:
	EOS_HLobbySearch Handle;
public:
	FEOSHLobbySearch()
		: Handle(nullptr)
	{
	}

	FEOSHLobbySearch(const EOS_HLobbySearch& data)
		: Handle(data)
	{
	}

public:
	operator EOS_HLobbySearch() const { return Handle; }
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		DELEGATES
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyCreateLobbyCallback, const FEOSLobbyCreateLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyDestroyLobbyCallback, const FEOSLobbyDestroyLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyJoinLobbyCallback, const FEOSLobbyJoinLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyLeaveLobbyCallback, const FEOSLobbyLeaveLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyUpdateLobbyCallback, const FEOSLobbyUpdateLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyPromoteMemberCallback, const FEOSLobbyPromoteMemberCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyKickMemberCallback, const FEOSLobbyKickMemberCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyUpdateReceivedCallback, const FEOSLobbyUpdateReceivedCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyMemberUpdateReceivedCallback, const FEOSLobbyMemberUpdateReceivedCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyMemberStatusReceivedCallback, const FEOSLobbyMemberStatusReceivedCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyInviteReceivedCallback, const FEOSLobbyInviteReceivedCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbySendInviteCallback, const FEOSLobbySendInviteCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyRejectInviteCallback, const FEOSLobbyRejectInviteCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyQueryInvitesCallback, const FEOSLobbyQueryInvitesCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyFindCallback, const FEOSLobbySearchFindCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnLobbyInviteAcceptedCallback, const FEOSLobbyInviteAcceptedCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnJoinLobbyAcceptedCallback, const FEOSLobbyJoinLobbyAcceptedCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnOnRTCRoomConnectionChangedCallback, const FEOSLobbyRTCRoomConnectionChangedCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyCreateLobbyCallbackDelegate, const FEOSLobbyCreateLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyDestroyLobbyCallbackDelegate, const FEOSLobbyDestroyLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyJoinLobbyCallbackDelegate, const FEOSLobbyJoinLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyLeaveLobbyCallbackDelegate, const FEOSLobbyLeaveLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyUpdateLobbyCallbackDelegate, const FEOSLobbyUpdateLobbyCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyPromoteMemberCallbackDelegate, const FEOSLobbyPromoteMemberCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyKickMemberCallbackDelegate, const FEOSLobbyKickMemberCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyUpdateReceivedCallbackDelegate, const FEOSLobbyUpdateReceivedCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyMemberUpdateReceivedCallbackDelegate, const FEOSLobbyMemberUpdateReceivedCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyMemberStatusReceivedCallbackDelegate, const FEOSLobbyMemberStatusReceivedCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyInviteReceivedCallbackDelegate, const FEOSLobbyInviteReceivedCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbySendInviteCallbackDelegate, const FEOSLobbySendInviteCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyRejectInviteCallbacDelegatek, const FEOSLobbyRejectInviteCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyQueryInvitesCallbackDelegate, const FEOSLobbyQueryInvitesCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyFindCallbackDelegate, const FEOSLobbySearchFindCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLobbyInviteAcceptedCallbackDelegate, const FEOSLobbyInviteAcceptedCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnOnRTCRoomConnectionChangedCallbackDelegate, const FEOSLobbyRTCRoomConnectionChangedCallbackInfo&, data);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		CALLBACK OBJECTS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

struct FCreateLobbyCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyCreateLobbyCallback m_Callback;
public:
	FCreateLobbyCallback(UCoreLobby* obj, const FOnLobbyCreateLobbyCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FDestroyLobbyCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyDestroyLobbyCallback m_Callback;
public:
	FDestroyLobbyCallback(UCoreLobby* obj, const FOnLobbyDestroyLobbyCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FJoinLobbyCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyJoinLobbyCallback m_Callback;
public:
	FJoinLobbyCallback(UCoreLobby* obj, const FOnLobbyJoinLobbyCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FLeaveLobbyCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyLeaveLobbyCallback m_Callback;
public:
	FLeaveLobbyCallback(UCoreLobby* obj, const FOnLobbyLeaveLobbyCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FUpdateLobbyCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyUpdateLobbyCallback m_Callback;
public:
	FUpdateLobbyCallback(UCoreLobby* obj, const FOnLobbyUpdateLobbyCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FLobbyPromoteMemberCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyPromoteMemberCallback m_Callback;
public:
	FLobbyPromoteMemberCallback(UCoreLobby* obj, const FOnLobbyPromoteMemberCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FLobbyKickMemberCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyKickMemberCallback m_Callback;
public:
	FLobbyKickMemberCallback(UCoreLobby* obj, const FOnLobbyKickMemberCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FLobbySendInviteCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbySendInviteCallback m_Callback;
public:
	FLobbySendInviteCallback(UCoreLobby* obj, const FOnLobbySendInviteCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FLobbyRejectInviteCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyRejectInviteCallback m_Callback;
public:
	FLobbyRejectInviteCallback(UCoreLobby* obj, const FOnLobbyRejectInviteCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FLobbyQueryInvitesCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyQueryInvitesCallback m_Callback;
public:
	FLobbyQueryInvitesCallback(UCoreLobby* obj, const FOnLobbyQueryInvitesCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FLobbyFindCallback
{
public:
	UCoreLobby* m_LobbyObject;
	FOnLobbyFindCallback m_Callback;
public:
	FLobbyFindCallback(UCoreLobby* obj, const FOnLobbyFindCallback& callback)
		: m_LobbyObject(obj)
		, m_Callback(callback)
	{
	}
};

struct FLobbyUpdateReceivedCallback
{
public:
	FLobbyUpdateReceivedCallback(UObject* WorldContextObject, const FOnLobbyUpdateReceivedCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FLobbyUpdateReceivedCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnLobbyUpdateReceivedCallback m_Callback;
};

struct FLobbyMemberUpdateReceivedCallback
{
public:
	FLobbyMemberUpdateReceivedCallback(UObject* WorldContextObject, const FOnLobbyMemberUpdateReceivedCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FLobbyMemberUpdateReceivedCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnLobbyMemberUpdateReceivedCallback m_Callback;
};

struct FLobbyMemberStatusReceivedCallback
{
public:
	FLobbyMemberStatusReceivedCallback(UObject* WorldContextObject, const FOnLobbyMemberStatusReceivedCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FLobbyMemberStatusReceivedCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnLobbyMemberStatusReceivedCallback m_Callback;
};

struct FLobbyInviteAcceptedCallback
{
public:
	FLobbyInviteAcceptedCallback(UObject* WorldContextObject, const FOnLobbyInviteAcceptedCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FLobbyInviteAcceptedCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnLobbyInviteAcceptedCallback m_Callback;
};

struct FJoinLobbyAcceptedCallback
{
public:
	FJoinLobbyAcceptedCallback(UObject* WorldContextObject, const FOnJoinLobbyAcceptedCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FJoinLobbyAcceptedCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnJoinLobbyAcceptedCallback m_Callback;
};

struct FLobbyRTCRoomConnectionChanged
{
public:
	FLobbyRTCRoomConnectionChanged(UObject* WorldContextObject, const FOnOnRTCRoomConnectionChangedCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FLobbyRTCRoomConnectionChanged()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnOnRTCRoomConnectionChangedCallback m_Callback;
};
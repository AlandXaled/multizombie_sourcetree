/**
* Copyright (C) 2017-2021 | eelDev
*
* Official EOSCore Documentation: https://eeldev.com
*/

#pragma once

#include "CoreMinimal.h"

#include "eos_rtc_audio_types.h"
#include "Core/EOSHelpers.h"
#include "Core/EOSTypes.h"
#include "EOSRTCAudioTypes.generated.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		ENUMS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

/**
* An enumeration of the different audio channel statuses.
*/
UENUM(BlueprintType)
enum class EEOSERTCAudioStatus : uint8
{
	/** Audio unsupported by the source (no devices) */
	EOS_RTCAS_Unsupported = 0,
	/** Audio enabled */
	EOS_RTCAS_Enabled = 1,
	/** Audio disabled */
	EOS_RTCAS_Disabled = 2,
	/** Audio disabled by the administrator */
	EOS_RTCAS_AdminDisabled = 3,
	/** Audio channel is disabled temporarily for both sending and receiving */
	EOS_RTCAS_NotListeningDisabled = 4
};

/**
* An enumeration of the different audio input device statuses.
*/
UENUM(BlueprintType)
enum class EEOSERTCAudioInputStatus : uint8
{
	/** The device is not in used right now (e.g: you are alone in the room). In such cases, the hardware resources are not allocated. */
	EOS_RTCAIS_Idle = 0,
	/** The device is being used and capturing audio */
	EOS_RTCAIS_Recording = 1,
	/**
	* The SDK is in a recording state, but actually capturing silence because the device is exclusively being used by the platform at the moment.
	* This only applies to certain platforms.
	*/
	EOS_RTCAIS_RecordingSilent = 2,
	/**
	* The SDK is in a recording state, but actually capturing silence because the device is disconnected (e.g: the microphone is not plugged in).
	* This only applies to certain platforms.
	*/
	EOS_RTCAIS_RecordingDisconnected = 3,
	/** Something failed while trying to use the device */
	EOS_RTCAIS_Failed = 4
};

/**
* An enumeration of the different audio output device statuses.
*/
UENUM(BlueprintType)
enum class EEOSERTCAudioOutputStatus : uint8
{
	/** The device is not in used right now (e.g: you are alone in the room). In such cases, the hardware resources are not allocated. */
	EOS_RTCAOS_Idle = 0,
	/** Device is in use */
	EOS_RTCAOS_Playing = 1,
	/** Something failed while trying to use the device */
	EOS_RTCAOS_Failed = 2
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		STRUCTS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

/**
* This struct is used to inform the audio system of a user.
*/
USTRUCT(BlueprintType)
struct FEOSRegisterPlatformAudioUserOptions
{
	GENERATED_BODY()
public:
	/** Platform dependent user id. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString UserId;
public:
	explicit FEOSRegisterPlatformAudioUserOptions() = default;
};


/**
* This struct is used to remove a user from the audio system.
*/
USTRUCT(BlueprintType)
struct FEOSUnregisterPlatformAudioUserOptions
{
	GENERATED_BODY()
public:
	/** Platform dependent user id. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString UserId;
public:
	explicit FEOSUnregisterPlatformAudioUserOptions() = default;
};

/**
* This struct is used to call EOS_RTCAudio_AddNotifyParticipantUpdated.
*/
USTRUCT(BlueprintType)
struct FEOSAddNotifyParticipantUpdatedOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user trying to request this operation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The  room this event is registered on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString RoomName;
public:
	explicit FEOSAddNotifyParticipantUpdatedOptions() = default;
};

/**
 * This struct is passed in with a call to EOS_RTCAudio_AddNotifyParticipantUpdated registered event.
 */
USTRUCT(BlueprintType)
struct FEOSJoinRoomCallbackInfo
{
	GENERATED_BODY()
public:
	/** Client-specified data passed into EOS_RTCAudio_AddNotifyParticipantUpdated. */
	void* ClientData;
	/** The Product User ID of the user who initiated this request. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The room associated with this event. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString RoomName;
	/** The participant updated. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId ParticipantId;
	/** The participant speaking / non-speaking status. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bSpeaking;
	/** The participant audio status (enabled, disabled). */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSERTCAudioStatus AudioStatus;
public:
	FEOSJoinRoomCallbackInfo() = default;

	FEOSJoinRoomCallbackInfo(const EOS_RTCAudio_ParticipantUpdatedCallbackInfo& data)
		: ClientData(data.ClientData)
		, LocalUserId(data.LocalUserId)
		, RoomName(UTF8_TO_TCHAR(data.RoomName))
		, ParticipantId(data.ParticipantId)
		, bSpeaking(data.bSpeaking > 0)
		, AudioStatus(static_cast<EEOSERTCAudioStatus>(data.AudioStatus))
	{
	}
};

/**
* This struct is used to call EOS_RTCAudio_AddNotifyAudioDevicesChanged.
*/
USTRUCT(BlueprintType)
struct FEOSAddNotifyAudioDevicesChangedOptions
{
	GENERATED_BODY()
public:
	explicit FEOSAddNotifyAudioDevicesChangedOptions() = default;
};

/**
* This struct is passed in with a call to EOS_RTCAudio_AddNotifyAudioDevicesChanged registered event.
*/
USTRUCT(BlueprintType)
struct FEOSAudioDevicesChangedCallbackInfo
{
	GENERATED_BODY()
public:
	/** Client-specified data passed into EOS_RTCAudio_AddNotifyAudioDevicesChanged. */
	void* ClientData;
public:
	FEOSAudioDevicesChangedCallbackInfo() = default;

	FEOSAudioDevicesChangedCallbackInfo(const EOS_RTCAudio_AudioDevicesChangedCallbackInfo& data)
		: ClientData(data.ClientData)
	{}
};

/**
* Input parameters for the EOS_RTCAudio_GetAudioInputDevicesCount function.
*/
USTRUCT(BlueprintType)
struct FEOSGetAudioInputDevicesCountOptions
{
	GENERATED_BODY()
public:
	explicit FEOSGetAudioInputDevicesCountOptions() = default;
};

/**
* Input parameters for the EOS_RTCAudio_GetAudioInputDeviceByIndex function.
*/
USTRUCT(BlueprintType)
struct FEOSGetAudioInputDeviceByIndexOptions
{
	GENERATED_BODY()
public:
	/** Index of the device info to retrieve. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	int32 DeviceInfoIndex;
public:
	explicit FEOSGetAudioInputDeviceByIndexOptions() = default;
};

/**
* This struct is used to get information about a specific input device.
*/
USTRUCT(BlueprintType)
struct FEOSAudioInputDeviceInfo
{
	GENERATED_BODY()
public:
	/** True if this is the default audio input device in the system. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	bool bDefaultDevice;
	/** 
	*  The persistent unique id of the device.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString DeviceId;
	/**
	* The name of the device
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString DeviceName;
public:
	explicit FEOSAudioInputDeviceInfo() = default;
	FEOSAudioInputDeviceInfo(const EOS_RTCAudio_AudioInputDeviceInfo& data)
		: bDefaultDevice(data.bDefaultDevice > 0)
		, DeviceId(UTF8_TO_TCHAR(data.DeviceId))
		, DeviceName(UTF8_TO_TCHAR(data.DeviceName))
	{}
};

/**
 * Output parameters for the EOS_RTCAudio_GetAudioOutputDevicesCount function.
 */
USTRUCT(BlueprintType)
struct FEOSGetAudioOutputDevicesCountOptions
{
	GENERATED_BODY()
public:
	explicit FEOSGetAudioOutputDevicesCountOptions() = default;
};

/**
* Input parameters for the EOS_RTCAudio_GetAudioOutputDeviceByIndex function.
*/
USTRUCT(BlueprintType)
struct FEOSGetAudioOutputDeviceByIndexOptions
{
	GENERATED_BODY()
public:
	/** Index of the device info to retrieve. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	int32 DeviceInfoIndex;
public:
	explicit FEOSGetAudioOutputDeviceByIndexOptions() = default;
};

/**
* This struct is used to get information about a specific input device.
*/
USTRUCT(BlueprintType)
struct FEOSAudioOutputDeviceInfo
{
	GENERATED_BODY()
public:
	/** True if this is the default audio input device in the system. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	bool bDefaultDevice;
	/** 
	*  The persistent unique id of the device.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString DeviceId;
	/**
	* The name of the device
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString DeviceName;
public:
	explicit FEOSAudioOutputDeviceInfo() = default;
	FEOSAudioOutputDeviceInfo(const EOS_RTCAudio_AudioOutputDeviceInfo& data)
		: bDefaultDevice(data.bDefaultDevice > 0)
		, DeviceId(UTF8_TO_TCHAR(data.DeviceId))
		, DeviceName(UTF8_TO_TCHAR(data.DeviceName))
	{}
};

/**
* This struct is used to call EOS_RTCAudio_SetAudioInputSettings.
*/
USTRUCT(BlueprintType)
struct FEOSSetAudioInputSettingsOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user trying to request this operation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The device Id to be used for this user. Pass NULL or empty string to use default input device. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString DeviceId;
	/** The volume to be configured for this device (range 0.0 to 100.0).
	* At the moment, the only value that produce any effect is 0.0 (silence). Any other value is ignored and causes no change to the volume.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	float Volume;
	/** Enable or disable Platform AEC (Acoustic Echo Cancellation) if available. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	bool bPlatformAEC;
public:
	explicit FEOSSetAudioInputSettingsOptions() = default;
};

/**
* This struct is used to call EOS_RTCAudio_SetAudioOutputSettings.
*/
USTRUCT(BlueprintType)
struct FEOSSetAudioOutputSettingsOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user who initiated this request. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The device Id to be used for this user. Pass NULL or empty string to use default output device. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString DeviceId;
	/** The volume to be configured for this device (range 0.0 to 100.0). Volume 50 means that the audio volume is not modified
	* and stays in its source value.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	float Volume;
public:
	explicit FEOSSetAudioOutputSettingsOptions() = default;
};

/**
* This struct is used to call EOS_RTCAudio_AddNotifyAudioInputState.
*/
USTRUCT(BlueprintType)
struct FEOSAddNotifyAudioInputStateOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user trying to request this operation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The room this event is registered on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString RoomName;
public:
	explicit FEOSAddNotifyAudioInputStateOptions() = default;
};

/**
* This struct is passed in with a call to EOS_RTCAudio_AddNotifyAudioInputState registered event.
*/
USTRUCT(BlueprintType)
struct FEOSAudioInputStateCallbackInfo
{
	GENERATED_BODY()
public:
	/** Client-specified data passed into EOS_RTCAudio_AddNotifyAudioInputState. */
	void* ClientData;
	/** The Product User ID of the user who initiated this request. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The room associated with this event. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString RoomName;
	/** The status of the audio input. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSERTCAudioInputStatus Status;
public:
	FEOSAudioInputStateCallbackInfo() = default;

	FEOSAudioInputStateCallbackInfo(const EOS_RTCAudio_AudioInputStateCallbackInfo& data)
		: ClientData(data.ClientData)
		, LocalUserId(data.LocalUserId)
		, RoomName(UTF8_TO_TCHAR(data.RoomName))
		, Status(static_cast<EEOSERTCAudioInputStatus>(data.Status))
	{
	}
};

/**
* This struct is used to call EOS_RTCAudio_AddNotifyAudioOutputState.
*/
USTRUCT(BlueprintType)
struct FEOSAddNotifyAudioOutputStateOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user trying to request this operation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The  room this event is registered on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString RoomName;
public:
	explicit FEOSAddNotifyAudioOutputStateOptions() = default;
};

/**
* This struct is passed in with a call to EOS_RTCAudio_AddNotifyAudioOutputState registered event.
*/
USTRUCT(BlueprintType)
struct FEOSOutputStateCallbackInfo
{
	GENERATED_BODY()
public:
	/** Client-specified data passed into EOS_RTCAudio_AddNotifyAudioOutputState. */
	void* ClientData;
	/** The Product User ID of the user who initiated this request. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The room associated with this event. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString RoomName;
	/** The status of the audio output. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSERTCAudioOutputStatus Status;
public:
	FEOSOutputStateCallbackInfo() = default;

	FEOSOutputStateCallbackInfo(const EOS_RTCAudio_AudioOutputStateCallbackInfo& data)
		: ClientData(data.ClientData)
		, LocalUserId(data.LocalUserId)
		, RoomName(UTF8_TO_TCHAR(data.RoomName))
		, Status(static_cast<EEOSERTCAudioOutputStatus>(data.Status))
	{
	}
};

/**
* This struct is used to call EOS_RTCAudio_AddNotifyAudioBeforeSend.
*/
USTRUCT(BlueprintType)
struct FEOSAddNotifyAudioBeforeSendOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user trying to request this operation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The  room this event is registered on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString RoomName;
public:
	explicit FEOSAddNotifyAudioBeforeSendOptions() = default;
};

/**
* This struct is used to represent an audio buffer received in callbacks from EOS_RTCAudio_AddNotifyAudioBeforeSend and EOS_RTCAudio_AddNotifyAudioBeforeRender.
*/
USTRUCT(BlueprintType)
struct FEOSRTCAudioAudioBuffer
{
	GENERATED_BODY()
public:
	/** Pointer to the data with the interleaved audio frames in signed 16 bits format. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	TArray<int32> Frames;
	/** Sample rate for the samples in the Frames buffer. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 SampleRate;
	/** Number of channels for the samples in the Frames buffer. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	int32 Channels;
public:
	FEOSRTCAudioAudioBuffer() = default;

	FEOSRTCAudioAudioBuffer(const EOS_RTCAudio_AudioBuffer* data)
	{
		for (uint32_t i=0; i<data->FramesCount; i++)
		{
			Frames.Add(data->Frames[i]);
		}
	}
public:
	operator EOS_RTCAudio_AudioBuffer() const
	{
		EOS_RTCAudio_AudioBuffer Data;
		Data.ApiVersion = EOS_RTCAUDIO_AUDIOBUFFER_API_LATEST;
		Data.FramesCount = Frames.Num();
		Data.SampleRate = SampleRate;
		Data.Channels = Channels;
		Data.Frames = (int16_t*)(Frames.GetData());

		return Data;
	}
};

/**
* This struct is passed in with a call to EOS_RTCAudio_AddNotifyAudioBeforeSend registered event.
*/
USTRUCT(BlueprintType)
struct FEOSBeforeSendCallbackInfo
{
	GENERATED_BODY()
public:
	/** Client-specified data passed into EOS_RTCAudio_AddNotifyAudioOutputState. */
	void* ClientData;
	/** The Product User ID of the user who initiated this request. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The room associated with this event. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString RoomName;
	/** The status of the audio output. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSRTCAudioAudioBuffer Buffer;
public:
	FEOSBeforeSendCallbackInfo() = default;

	FEOSBeforeSendCallbackInfo(const EOS_RTCAudio_AudioBeforeSendCallbackInfo& data)
		: ClientData(data.ClientData)
		, LocalUserId(data.LocalUserId)
		, RoomName(UTF8_TO_TCHAR(data.RoomName))
		, Buffer(data.Buffer)
	{
	}
};

/**
* This struct is used to call EOS_RTCAudio_AddNotifyAudioBeforeRender.
*/
USTRUCT(BlueprintType)
struct FEOSAddNotifyAudioBeforeRenderOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user trying to request this operation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The  room this event is registered on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString RoomName;
	/**
	* Mixed audio or unmixed audio.
	* For unmixed audio notifications it is not supported to modify the samples in the callback.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	bool bUnmixedAudio;
public:
	explicit FEOSAddNotifyAudioBeforeRenderOptions() = default;
};

/**
* This struct is passed in with a call to EOS_RTCAudio_AddNotifyAudioBeforeRender registered event.
*/
USTRUCT(BlueprintType)
struct FEOSBeforeRenderCallbackInfo
{
	GENERATED_BODY()
public:
	/** Client-specified data passed into EOS_RTCAudio_AddNotifyAudioOutputState. */
	void* ClientData;
	/** The Product User ID of the user who initiated this request. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The room associated with this event. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString RoomName;
	/**
	* Audio buffer.
	* If bUnmixedAudio was set to true when setting the notifications (aka: you get buffers per participant), then you should
	* not modify this buffer.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSRTCAudioAudioBuffer Buffer;
	/**
	* The Product User ID of the participant if bUnmixedAudio was set to true when setting the notifications, or empty if
	* bUnmixedAudio was set to false and thus the buffer is the mixed audio of all participants
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId ParticipantId;
public:
	FEOSBeforeRenderCallbackInfo() = default;

	FEOSBeforeRenderCallbackInfo(const EOS_RTCAudio_AudioBeforeRenderCallbackInfo& data)
		: ClientData(data.ClientData)
		, LocalUserId(data.LocalUserId)
		, RoomName(UTF8_TO_TCHAR(data.RoomName))
		, Buffer(data.Buffer)
		, ParticipantId(data.ParticipantId)
	{
	}
};

/**
* This struct is used to call EOS_RTCAudio_SendAudio.
*/
USTRUCT(BlueprintType)
struct FEOSSendAudioOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user trying to request this operation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The  room this event is registered on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString RoomName;
	/**
	* Audio buffer, which must have a duration of 10 ms.
	* @note The SDK makes a copy of buffer. There is no need to keep the buffer around after calling EOS_RTCAudio_SendAudio
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSRTCAudioAudioBuffer Buffer;
public:
	explicit FEOSSendAudioOptions() = default;
};

/**
* This struct is passed in with a call to EOS_RTCAudio_UpdateSending
*/
USTRUCT(BlueprintType)
struct FEOSUpdateSendingOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user trying to request this operation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The  room this event is registered on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString RoomName;
	/** Muted or unmuted audio track status */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	EEOSERTCAudioStatus AudioStatus;
public:
	explicit FEOSUpdateSendingOptions() = default;
};

/**
* This struct is passed in with a call to EOS_RTCAudio_OnUpdateSendingCallback.
*/
USTRUCT(BlueprintType)
struct FEOSUpdateSendingCallbackInfo
{
	GENERATED_BODY()
public:
	/** This returns:
	* EOS_Success if the channel was successfully blocked.
	* EOS_UnexpectedError otherwise.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Client-specified data passed into EOS_RTCAudio_UpdateSending. */
	void* ClientData;
	/** The Product User ID of the user who initiated this request. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The room this settings should be applied on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString RoomName;
	/** Muted or unmuted audio track status */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EEOSERTCAudioStatus AudioStatus;
public:
	FEOSUpdateSendingCallbackInfo() = default;

	FEOSUpdateSendingCallbackInfo(const EOS_RTCAudio_UpdateSendingCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LocalUserId(data.LocalUserId)
		, RoomName(UTF8_TO_TCHAR(data.RoomName))
		, AudioStatus(static_cast<EEOSERTCAudioStatus>(data.AudioStatus))
	{
	}
};

/**
* This struct is passed in with a call to EOS_RTCAudio_UpdateReceiving.
*/
USTRUCT(BlueprintType)
struct FEOSUpdateReceivingOptions
{
	GENERATED_BODY()
public:
	/** The Product User ID of the user trying to request this operation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId LocalUserId;
	/** The room this settings should be applied on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FString RoomName;
	/** The participant to modify or null to update the global configuration */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	FEOSProductUserId ParticipantId;
	/** Mute or unmute audio track */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTC")
	bool bAudioEnabled;
public:
	explicit FEOSUpdateReceivingOptions() = default;
};

/**
* This struct is passed in with a call to EOS_RTCAudio_OnUpdateReceivingCallback.
*/
USTRUCT(BlueprintType)
struct FEOSUpdateReceivingCallbackInfo
{
	GENERATED_BODY()
public:
	/** This returns:
	* EOS_Success if the channel was successfully blocked.
	* EOS_UnexpectedError otherwise.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	EOSResult ResultCode;
	/** Client-specified data passed into EOS_RTCAudio_UpdateSending. */
	void* ClientData;
	/** The Product User ID of the user who initiated this request. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId LocalUserId;
	/** The room this settings should be applied on. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FString RoomName;
	/** The participant to modify or null to update the global configuration */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	FEOSProductUserId ParticipantId;
	/** Muted or unmuted audio track */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lobby")
	bool bAudioEnabled;
public:
	FEOSUpdateReceivingCallbackInfo() = default;

	FEOSUpdateReceivingCallbackInfo(const EOS_RTCAudio_UpdateReceivingCallbackInfo& data)
		: ResultCode(EOSHelpers::ToEOSCoreResult(data.ResultCode))
		, ClientData(data.ClientData)
		, LocalUserId(data.LocalUserId)
		, RoomName(UTF8_TO_TCHAR(data.RoomName))
		, ParticipantId(data.ParticipantId)
		, bAudioEnabled(data.bAudioEnabled > 0)
	{
	}
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		DELEGATES
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnParticipantUpdatedCallback, const FEOSJoinRoomCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAudioInputStateCallback, const FEOSAudioInputStateCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAudioOutputStateCallback, const FEOSOutputStateCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAudioBeforeSendCallback, const FEOSBeforeSendCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAudioBeforeRenderCallback, const FEOSBeforeRenderCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnUpdateSendingCallback, const FEOSUpdateSendingCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnUpdateReceivingCallback, const FEOSUpdateReceivingCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAudioDevicesChangedCallback, const FEOSAudioDevicesChangedCallbackInfo&, data);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		CALLBACK OBJECTS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

struct FAudioBeforeRenderCallback
{
public:
	FAudioBeforeRenderCallback(UObject* WorldContextObject, const FOnAudioBeforeRenderCallback& Callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(Callback)
	{
	}

	~FAudioBeforeRenderCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAudioBeforeRenderCallback m_Callback;
};

struct FAudioBeforeSendCallback
{
public:
	FAudioBeforeSendCallback(UObject* WorldContextObject, const FOnAudioBeforeSendCallback& Callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(Callback)
	{
	}

	~FAudioBeforeSendCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAudioBeforeSendCallback m_Callback;
};

struct FAudioOutputStateCallback
{
public:
	FAudioOutputStateCallback(UObject* WorldContextObject, const FOnAudioOutputStateCallback& Callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(Callback)
	{
	}

	~FAudioOutputStateCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAudioOutputStateCallback m_Callback;
};

struct FAudioInputStateCallback
{
public:
	FAudioInputStateCallback(UObject* WorldContextObject, const FOnAudioInputStateCallback& Callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(Callback)
	{
	}

	~FAudioInputStateCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAudioInputStateCallback m_Callback;
};

struct FAudioDevicesChangedCallback
{
public:
	FAudioDevicesChangedCallback(UObject* WorldContextObject, const FOnAudioDevicesChangedCallback& Callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(Callback)
	{
	}

	~FAudioDevicesChangedCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAudioDevicesChangedCallback m_Callback;
};

struct FParticipantUpdatedCallback
{
public:
	FParticipantUpdatedCallback(UObject* WorldContextObject, const FOnParticipantUpdatedCallback& Callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(Callback)
	{
	}

	~FParticipantUpdatedCallback()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnParticipantUpdatedCallback m_Callback;
};

struct FUpdateReceivingOptionsCallback
{
public:
	UObject* m_Object;
	FOnUpdateReceivingCallback m_Callback;
public:
	FUpdateReceivingOptionsCallback(UObject* obj, const FOnUpdateReceivingCallback& callback)
		: m_Object(obj)
		, m_Callback(callback)
	{
	}
};

struct FUpdateSendingOptionsCallback
{
public:
	UObject* m_Object;
	FOnUpdateSendingCallback m_Callback;
public:
	FUpdateSendingOptionsCallback(UObject* obj, const FOnUpdateSendingCallback& callback)
		: m_Object(obj)
		, m_Callback(callback)
	{
	}
};
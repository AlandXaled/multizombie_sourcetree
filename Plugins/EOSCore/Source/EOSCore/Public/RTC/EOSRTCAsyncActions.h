/**
* Copyright (C) 2017-2021 | eelDev
*
* Official EOSCore Documentation: https://eeldev.com
*/

#pragma once

#include "Core/EOSCoreAsync.h"
//#include "EOSRTCAsyncActions.generated.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		UEOSCoreRTCLogin
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//UCLASS()
//class EOSCORE_API UEOSCoreRTCLogin : public UEOSCoreAsyncAction
//{
//	GENERATED_BODY()
//public:
//	UPROPERTY(BlueprintAssignable)
//	FOnRTCLoginCallbackDelegate OnCallback;
//private:
//	FOnRTCLoginCallback m_Callback;
//public:
//	UEOSCoreRTCLogin() { m_Callback.BindUFunction(this, "HandleCallback"); }
//	~UEOSCoreRTCLogin() { m_Callback.Unbind(); }
//public:
//	/**
//	* Login/RTCenticate with user credentials.
//	*
//	* @param Options structure containing the account credentials to use during the login operation
//	*/
//	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject", Category = "EOSCore|RTC|Async", DisplayName = "EOS_RTC_Login"))
//	static UEOSCoreRTCLogin* EOSRTCLoginAsync(UObject* WorldContextObject, FEOSRTCLoginOptions Options);
//protected:
//	FEOSRTCLoginOptions m_Options;
//public:
//	virtual void Activate() override;
//private:
//	UFUNCTION()
//	void HandleCallback(const FEOSRTCLoginCallbackInfo& Data, bool bWasSuccessful)
//	{
//		OnCallback.Broadcast(Data);
//		m_Callback.Unbind();
//		SetReadyToDestroy();
//	}
//};
/**
* Copyright (C) 2017-2021 | eelDev
*
 official EOSCore Documentation: https://eeldev.com
*/

#pragma once

#include "CoreMinimal.h"
#include "EOSCoreModule.h"
#include "eos_ui_types.h"
#include "UI/EOSUITypes.h"
#include "EOSUI.generated.h"

/**
* The UI Interface is used to access the Social Overlay UI.  Each UI component will have a function for
* opening it.  All UI Interface calls take a handle of type EOS_HUI as the first parameter.
* This handle can be retrieved from an EOS_HPlatform handle by using the EOS_Platform_GetUIInterface function.
*
* @see EOS_Platform_GetUIInterface
*/

UCLASS()
class EOSCORE_API UCoreUI : public UEOSCoreSubsystem
{
	GENERATED_BODY()
public:
	UCoreUI();
public:
	/**
	 * The UI Interface is used to access the overlay UI.  Each UI component will have a function for
	 * opening it.  All UI Interface calls take a handle of type EOS_HUI as the first parameter.
	 * This handle can be retrieved from a EOS_HPlatform handle by using the EOS_Platform_GetUIInterface function.
	 *
	 * NOTE: At this time, this feature is only available for products that are part of the Epic Games store.
	 *
	 * @see EOS_Platform_GetUIInterface
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "EOSCore|Subsystems")
	static UCoreUI* GetUI() { return s_UIObject; }

public:
	/**
	* Opens the Social Overlay with a request to show the friends list.
	*
	* @param Options Structure containing the Epic Online Services Account ID of the friends list to show.
	* @param Callback A callback that is fired when the request to show the friends list has been sent to the Social Overlay, or on an error.
	*
	* @return EOS_Success If the Social Overlay has been notified about the request.
	*         EOS_InvalidParameters If any of the options are incorrect.
	*         EOS_NotConfigured If the Social Overlay is not properly configured.
	*         EOS_NoChange If the Social Overlay is already visible.
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Show_Friends"))
	void EOSUIShowFriends(UObject* WorldContextObject, FEOSUIShowFriendsOptions Options, const FOnShowFriendsCallback& Callback);

	/**
	* Hides the active Social Overlay.
	*
	* @param Options Structure containing the Epic Online Services Account ID of the browser to close.
	* @param Callback A callback that is fired when the request to hide the friends list has been processed, or on an error.
	*
	* @return EOS_Success If the Social Overlay has been notified about the request.
	*         EOS_InvalidParameters If any of the options are incorrect.
	*         EOS_NotConfigured If the Social Overlay is not properly configured.
	*         EOS_NoChange If the Social Overlay is already hidden.
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Hide_Friends"))
	void EOSUIHideFriends(UObject* WorldContextObject, FEOSUIHideFriendsOptions Options, const FOnHideFriendsCallback& Callback);

	/**
	* Gets the friends overlay visibility.
	*
	* @param Options Structure containing the Epic Online Services Account ID of the friends Social Overlay owner.
	*
	* @return EOS_TRUE If the overlay is visible.
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Get_Friends_Visible"))
	static bool EOSUIGetFriendsVisible(UObject* WorldContextObject, FEOSUIGetFriendsVisibleOptions Options);

	/**
	* Updates the current Toggle Friends Key.  This key can be used by the user to toggle the friends
	* overlay when available. The default value represents `Shift + F3` as `((int32_t)EOS_UIK_Shift | (int32_t)EOS_UIK_F3)`.
	* The provided key should satisfy EOS_UI_IsValidKeyCombination. The value EOS_UIK_None is specially handled
	* by resetting the key binding to the system default.
	*
	* @param Options Structure containing the key combination to use.
	*
	* @return EOS_Success If the overlay has been notified about the request.
	*         EOS_InvalidParameters If any of the options are incorrect.
	*         EOS_NotConfigured If the overlay is not properly configured.
	*         EOS_NoChange If the key combination did not change.
	*
	* @see EOS_UI_IsValidKeyCombination
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Set_Toggle_Friends_Key"))
	static EOSResult EOSUISetToggleFriendsKey(UObject* WorldContextObject, FEOSUISetToggleFriendsKeyOptions Options);

	/**
	* Returns the current Toggle Friends Key.  This key can be used by the user to toggle the friends
	* overlay when available. The default value represents `Shift + F3` as `((int32_t)EOS_UIK_Shift | (int32_t)EOS_UIK_F3)`.
	*
	* @param Options Structure containing any options that are needed to retrieve the key.
	* @return A valid key combination which represent a single key with zero or more modifier keys.
	*         EOS_UIK_None will be returned if any error occurs.
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Get_Toggle_Friends_Key"))
	static int32 EOSUIGetToggleFriendsKey(UObject* WorldContextObject, FEOSUIGetToggleFriendsKeyOptions Options);

	/**
	* Determine if a key combination is valid. A key combinations must have a single key and at least one modifier.
	* The single key must be one of the following: F1 through F12, Space, Backspace, Escape, or Tab.
	* The modifier key must be one or more of the following: Shift, Control, or Alt.
	*
	* @param KeyCombination The key to test.
	* @return  EOS_TRUE if the provided key combination is valid.
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_IsValid_Key_Combination"))
	static bool EOSUIIsValidKeyCombination(UObject* WorldContextObject, int32 KeyCombination);

	/**
	* Define any preferences for any display settings.
	*
	* @param Options Structure containing any options that are needed to set
	* 
	* @return EOS_Success If the overlay has been notified about the request.
	*         EOS_InvalidParameters If any of the options are incorrect.
	*         EOS_NotConfigured If the overlay is not properly configured.
	*         EOS_NoChange If the preferences did not change.
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Set_Display_Preference"))
	static EOSResult EOSUISetDisplayPreference(UObject* WorldContextObject, FEOSUISetDisplayPreferenceOptions Options);

	/**
	* Returns the current notification location display preference.
	* @return The current notification location display preference.
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Get_Notification_Location_Preference"))
	static EEOSUIENotificationLocation EOSUIGetNotificationLocationPreference(UObject* WorldContextObject);

	/**
	* Lets the SDK know that the given UI event ID has been acknowledged and should be released.
	*
	* @return An EOS_EResult is returned to indicate success or an error.
	*
	* EOS_Success is returned if the UI event ID has been acknowledged.
	* EOS_NotFound is returned if the UI event ID does not exist.
	*
	* @see EOS_Presence_JoinGameAcceptedCallbackInfo
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Acknowledge_EventId"))
	static EOSResult EOSUIAcknowledgeEventId(UObject* WorldContextObject, FEOSUIAcknowledgeEventIdOptions Options);

	/**
	* Register to receive notifications when the overlay display settings are updated.
	* Newly registered handlers will always be called the next tick with the current state.
	* @note must call RemoveNotifyDisplaySettingsUpdated to remove the notification.
	*
	* @param Options Structure containing information about the request.
	* @param Callback A callback that is fired when the overlay display settings are updated.
	*
	* @return handle representing the registered callback
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Add_Notify_Display_Settings_Updated"))
	static FEOSNotificationId EOSUIAddNotifyDisplaySettingsUpdated(UObject* WorldContextObject, const FEOSUIAddNotifyDisplaySettingsUpdatedOptions Options, const FOnDisplaySettingsUpdatedCallback& Callback);

	/**
	* Unregister from receiving notifications when the overlay display settings are updated.
	*
	* @param Id Handle representing the registered callback
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|UI", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_UI_Remove_Notify_Display_Settings_Updated"))
	static void EOSUIRemoveNotifyDisplaySettingsUpdated(UObject* WorldContextObject, FEOSNotificationId Id);
private:
	static void Internal_OnShowFriendsCallback(const EOS_UI_ShowFriendsCallbackInfo* Data);
	static void Internal_OnHideFriendsCallback(const EOS_UI_HideFriendsCallbackInfo* Data);
private:
	static UCoreUI* s_UIObject;
};

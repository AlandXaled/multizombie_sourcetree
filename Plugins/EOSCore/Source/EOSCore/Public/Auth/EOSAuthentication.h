/**
* Copyright (C) 2017-2021 | eelDev
*
* Official EOSCore Documentation: https://eeldev.com
*/

#pragma once

#include "CoreMinimal.h"
#include "EOSCoreModule.h"
#include "Auth/EOSAuthTypes.h"
#include "EOSAuthentication.generated.h"

/**
 * The Auth Interface is used to manage local user permissions and access to backend services through the verification of various forms of credentials.
 * All Auth Interface calls take a handle of type EOS_HAuth as the first parameter.
 * This handle can be retrieved from a EOS_HPlatform handle by using the EOS_Platform_GetAuthInterface function.
 *
 * @see EOS_Platform_GetAuthInterface
 */

UCLASS()
class EOSCORE_API UCoreAuthentication : public UEOSCoreSubsystem
{
	GENERATED_BODY()
public:
	UCoreAuthentication();
public:
	/**
	 * The Auth Interface is used to manage local user permissions and access to backend services through the verification of various forms of credentials.
	 * All Auth Interface calls take a handle of type EOS_HAuth as the first parameter.
	 * This handle can be retrieved from a EOS_HPlatform handle by using the EOS_Platform_GetAuthInterface function.
	 *
	 * @see EOS_Platform_GetAuthInterface
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "EOSCore|Subsystems")
	static UCoreAuthentication* GetAuthentication() { return s_AuthObject; }

public:
	/**
	* Login/Authenticate with user credentials.
	*
	* @param Options structure containing the account credentials to use during the login operation
	* @param Callback a callback that is fired when the login operation completes, either successfully or in error
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Login"))
	void EOSAuthLogin(UObject* WorldContextObject, FEOSAuthLoginOptions Options, const FOnAuthLoginCallback& Callback);

	/**
	* Signs the player out of the online service.
	*
	* @param Options structure containing information about which account to log out.
	* @param Callback a callback that is fired when the logout operation completes, either successfully or in error
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Logout"))
	void EOSAuthLogout(UObject* WorldContextObject, FEOSAuthLogoutOptions Options, const FOnAuthLogoutCallback& Callback);

	/**
	* Link external account by continuing previous login attempt with a continuance token.
	*
	* On Desktop and Mobile platforms, the user will be presented the Epic Account Portal to resolve their identity.
	*
	* On Console, the user will login to their Epic Account using an external device, e.g. a mobile device or a desktop PC,
	* by browsing to the presented authentication URL and entering the device code presented by the game on the console.
	*
	* On success, the user will be logged in at the completion of this action.
	* This will commit this external account to the Epic Account and cannot be undone in the SDK.
	*
	* @param Options structure containing the account credentials to use during the link account operation
	* @param Callback a callback that is fired when the link account operation completes, either successfully or in error
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Link_Account"))
	void EOSAuthLinkAccount(UObject* WorldContextObject, FEOSAuthLinkAccountOptions Options, const FOnAuthLinkAccountCallback& Callback);

	/**
	* Deletes a previously received and locally stored persistent auth access token for the currently logged in user of the local device.
	*
	* On Desktop and Mobile platforms, the access token is deleted from the keychain of the local user and a backend request is made to revoke the token on the authentication server.
	* On Console platforms, even though the caller is responsible for storing and deleting the access token on the local device,
	* this function should still be called with the access token before its deletion to make the best effort in attempting to also revoke it on the authentication server.
	* If the function would fail on Console, the caller should still proceed as normal to delete the access token locally as intended.
	*
	* @param Options structure containing operation input parameters
	* @param Callback a callback that is fired when the deletion operation completes, either successfully or in error
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Delete_Persistent_Auth"))
	void EOSAuthDeletePersistentAuth(UObject* WorldContextObject, const FEOSAuthDeletePersistentAuthOptions& Options, const FOnAuthDeletePersistentAuthCallback& Callback);

	/**
	* Contact the backend service to verify validity of an existing user auth token.
	* This function is intended for server-side use only.
	*
	* @param Options structure containing information about the auth token being verified
	* @param Callback a callback that is fired when the logout operation completes, either successfully or in error
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Verify_User_Auth"))
	void EOSAuthVerifyUserAuth(UObject* WorldContextObject, const FEOSAuthVerifyUserAuthOptions& Options, const FOnAuthVerifyUserAuthCallback& Callback);

	/**
	* Fetch the number of accounts that are logged in.
	*
	* @return the number of accounts logged in.
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Get_Logged_In_Accounts_Count"))
	static int32 EOSAuthGetLoggedInAccountsCount(UObject* WorldContextObject);

	/**
	* Fetch an Epic Online Services Account ID that is logged in.
	*
	* @param Index An index into the list of logged in accounts. If the index is out of bounds, the returned Epic Online Services Account ID will be invalid.
	*
	* @return The Epic Online Services Account ID associated with the index passed
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Get_Logged_In_Account_By_Index"))
	static FEOSEpicAccountId EOSAuthGetLoggedInAccountByIndex(UObject* WorldContextObject, int32 Index);

	/**
	* Fetches the login status for an Epic Online Services Account ID.
	*
	* @param LocalUserId The Epic Online Services Account ID of the user being queried
	*
	* @return The enum value of a user's login status
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Get_Login_Status"))
	static EOSELoginStatus EOSAuthGetLoginStatus(UObject* WorldContextObject, FEOSEpicAccountId LocalUserId);

	/**
	* Fetches the login status for an account id.
	*
	* @param LocalUserId the account id of the user being queried
	*
	* @return the enum value of a user's login status
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "EOSCore|Authentication", meta = (DisplayName = "EOS_Auth_Get_Login_Status_(Pure)"))
	static EOSELoginStatus EOSAuthGetLoginStatusPure(UObject* WorldContextObject, const FEOSEpicAccountId LocalUserId) { return EOSAuthGetLoginStatus(WorldContextObject, LocalUserId); }

	/**
	* Fetches a user auth token for an Epic Online Services Account ID.
	*
	* @param Options Structure containing the api version of CopyUserAuthToken to use
	* @param LocalUserId The Epic Online Services Account ID of the user being queried
	* @param OutUserAuthToken The auth token for the given user, if it exists and is valid; use EOS_Auth_Token_Release when finished
	*
	* @see EOS_Auth_Token_Release
	*
	* @return EOS_Success if the information is available and passed out in OutUserAuthToken
	*         EOS_InvalidParameters if you pass a null pointer for the out parameter
	*         EOS_NotFound if the auth token is not found or expired.
	*
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Copy_User_Auth_Token"))
	static EOSResult EOSAuthCopyUserAuthToken(UObject* WorldContextObject, FEOSAuthCopyUserAuthTokenOptions Options, FEOSEpicAccountId LocalUserId, FEOSAuthToken& OutUserAuthToken);

	/**
	* Register to receive login status updates.
	* @note must call RemoveNotifyLoginStatusChanged to remove the notification
	*
	* @param Callback a callback that is fired when the login status for a user changes
	*
	* @return handle representing the registered callback
	*/
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Add_Notify_Login_Status_Changed"))
	static FEOSNotificationId EOSAuthAddNotifyLoginStatusChanged(UObject* WorldContextObject, const FOnAuthLoginStatusChangedCallback& Callback);

	/**
	 * Unregister from receiving login status updates.
	 *
	 * @param ID handle representing the registered callback
	 */
	UFUNCTION(BlueprintCallable, Category = "EOSCore|Authentication", meta = (WorldContext = "WorldContextObject", DisplayName = "EOS_Auth_Remove_Notify_Login_Status_Changed"))
	static void EOSAuthRemoveNotifyLoginStatusChanged(UObject* WorldContextObject, FEOSNotificationId ID);
private:
	static void Internal_OnAuthLoginCallback(const EOS_Auth_LoginCallbackInfo* Data);
	static void Internal_OnAuthLogoutCallback(const EOS_Auth_LogoutCallbackInfo* Data);
	static void Internal_OnAuthOnLinkAccountCallback(const EOS_Auth_LinkAccountCallbackInfo* Data);
	static void Internal_OnAuthDeletePersistentAuthCallback(const EOS_Auth_DeletePersistentAuthCallbackInfo* Data);
	static void Internal_OnAuthVerifyUserAuthCallback(const EOS_Auth_VerifyUserAuthCallbackInfo* Data);
private:
	static UCoreAuthentication* s_AuthObject;
};

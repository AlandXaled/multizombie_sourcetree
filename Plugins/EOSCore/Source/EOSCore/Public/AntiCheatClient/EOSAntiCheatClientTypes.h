/**
* Copyright (C) 2017-2021 | eelDev
*
* Official EOSCore Documentation: https://eeldev.com
*/

#pragma once

#include "CoreMinimal.h"
#include "eos_anticheatclient_types.h"
#include "Core/EOSCoreAntiCheatCommon.h"
#include "Core/EOSTypes.h"
#include "Core/EOSHelpers.h"
#include "EOSAntiCheatClientTypes.generated.h"

class UCoreAntiCheatClient;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		ENUMS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

/** Operating modes */
UENUM(BlueprintType)
enum class EEOSEAntiCheatClientMode : uint8
{
	/** Not used */
	EOS_ACCM_Invalid = 0,
	/** Dedicated or listen server mode */
	EOS_ACCM_ClientServer = 1,
	/** Full mesh peer-to-peer mode */
	EOS_ACCM_PeerToPeer = 2
};

/** Anti-cheat integrity violation types */
UENUM(BlueprintType)
enum class EEOSEAntiCheatClientViolationType : uint8
{
	/** Not used */
	EOS_ACCVT_Invalid = 0,
	/** An anti-cheat asset integrity catalog file could not be found */
	EOS_ACCVT_IntegrityCatalogNotFound = 1,
	/** An anti-cheat asset integrity catalog file is corrupt or invalid */
	EOS_ACCVT_IntegrityCatalogError = 2,
	/** An anti-cheat asset integrity catalog file's certificate has been revoked. */
	EOS_ACCVT_IntegrityCatalogCertificateRevoked = 3,
	/**
	* The primary anti-cheat asset integrity catalog does not include an entry for the game's
	* main executable, which is required.
	*/
	EOS_ACCVT_IntegrityCatalogMissingMainExecutable = 4,
	/** A disallowed game file modification was detected */
	EOS_ACCVT_GameFileMismatch = 5,
	/** A disallowed game file removal was detected */
	EOS_ACCVT_RequiredGameFileNotFound = 6,
	/** A disallowed game file addition was detected */
	EOS_ACCVT_UnknownGameFileForbidden = 7,
	/** A system file failed an integrity check */
	EOS_ACCVT_SystemFileUntrusted = 8,
	/** A disallowed code module was loaded into the game process */
	EOS_ACCVT_ForbiddenModuleLoaded = 9,
	/** A disallowed game process memory modification was detected */
	EOS_ACCVT_CorruptedMemory = 10,
	/** A disallowed tool was detected running in the system */
	EOS_ACCVT_ForbiddenToolDetected = 11,
	/** An internal anti-cheat integrity check failed */
	EOS_ACCVT_InternalAntiCheatViolation = 12,
	/** Integrity checks on messages between the game client and game server failed */
	EOS_ACCVT_CorruptedNetworkMessageFlow = 13,
	/** The game is running inside a disallowed virtual machine */
	EOS_ACCVT_VirtualMachineNotAllowed = 14,
	/** A forbidden operating system configuration was detected */
	EOS_ACCVT_ForbiddenSystemConfiguration = 15
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		STRUCTS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

/**
* Structure containing details about a new message that must be dispatched to the game server.
*/
USTRUCT(BlueprintType)
struct FEOSAntiCheatClientOnMessageToServerCallbackInfo
{
	GENERATED_BODY()
public:
	/** Caller-specified context data */
	void* ClientData;
	/** The message data that must be sent to the server */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Auth")
	FString MessageData;
	/** The size in bytes of MessageData */
	int32 MessageDataSizeBytes;

public:
	explicit FEOSAntiCheatClientOnMessageToServerCallbackInfo()
		: ClientData(nullptr)
		, MessageDataSizeBytes(0)
	{
	}

	FEOSAntiCheatClientOnMessageToServerCallbackInfo(const EOS_AntiCheatClient_OnMessageToServerCallbackInfo& data)
		: ClientData(data.ClientData)
		, MessageData(UTF8_TO_TCHAR(data.MessageData))
		, MessageDataSizeBytes(data.MessageDataSizeBytes)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientAddNotifyMessageToServerOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	explicit FEOSAntiCheatClientAddNotifyMessageToServerOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_ADDNOTIFYMESSAGETOSERVER_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientAddNotifyMessageToPeerOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	explicit FEOSAntiCheatClientAddNotifyMessageToPeerOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_ADDNOTIFYMESSAGETOPEER_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientAddNotifyPeerActionRequiredOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	explicit FEOSAntiCheatClientAddNotifyPeerActionRequiredOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_ADDNOTIFYPEERACTIONREQUIRED_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientAddNotifyPeerAuthStatusChangedOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	explicit FEOSAntiCheatClientAddNotifyPeerAuthStatusChangedOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_ADDNOTIFYPEERAUTHSTATUSCHANGED_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientBeginSessionOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Logged in user identifier from earlier call to EOS_Connect_Login family of functions */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSProductUserId LocalUserId;
	/** Operating mode */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	EEOSEAntiCheatClientMode Mode;
public:
	explicit FEOSAntiCheatClientBeginSessionOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_BEGINSESSION_API_LATEST)
		, Mode(EEOSEAntiCheatClientMode::EOS_ACCM_Invalid)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientEndSessionOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	explicit FEOSAntiCheatClientEndSessionOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_ENDSESSION_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientPollStatusOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** The size of OutMessage in bytes. Recommended size is 256 bytes. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 OutMessageLength;
public:
	explicit FEOSAntiCheatClientPollStatusOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_POLLSTATUS_API_LATEST)
		, OutMessageLength(256)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientAddExternalIntegrityCatalogOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** UTF-8 path to the .bin catalog file to add */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FString PathToBinFile;
public:
	explicit FEOSAntiCheatClientAddExternalIntegrityCatalogOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_ADDEXTERNALINTEGRITYCATALOG_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientReceiveMessageFromServerOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** The size of the data received */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 DataLengthBytes;
	/** The data received */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	TArray<uint8> Data;
public:
	explicit FEOSAntiCheatClientReceiveMessageFromServerOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_RECEIVEMESSAGEFROMSERVER_API_LATEST)
		, DataLengthBytes(0)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientGetProtectMessageOutputLengthOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Length in bytes of input */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 DataLengthBytes;
public:
	explicit FEOSAntiCheatClientGetProtectMessageOutputLengthOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_GETPROTECTMESSAGEOUTPUTLENGTH_API_LATEST)
		, DataLengthBytes(0)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientProtectMessageOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Length in bytes of input */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 DataLengthBytes;
	/** The data to encrypt */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	TArray<uint8> Data;
	/** The size in bytes of OutBuffer */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 OutBufferSizeBytes;
public:
	explicit FEOSAntiCheatClientProtectMessageOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_PROTECTMESSAGE_API_LATEST)
		, DataLengthBytes(0)
		, OutBufferSizeBytes(0)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientUnprotectMessageOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Length in bytes of input */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 DataLengthBytes;
	/** The data to decrypt */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	TArray<uint8> Data;
	/** The size in bytes of OutBuffer */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 OutBufferSizeBytes;
public:
	explicit FEOSAntiCheatClientUnprotectMessageOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_UNPROTECTMESSAGE_API_LATEST)
		, DataLengthBytes(0)
		, OutBufferSizeBytes(0)
	{
	}
};

/**
* A special peer handle that represents the client itself.
* It does not need to be registered or unregistered and is
* used in OnPeerActionRequiredCallback to quickly signal to the user
* that they will not be able to join online play.
*/
USTRUCT(BlueprintType)
struct FEOSAntiCheatClientRegisterPeerOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Locally unique value describing the remote user (e.g. a player object pointer) */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSAntiCheatCommonClientHandle PeerHandle;
	/** Type of remote user being registered */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	EEOSEAntiCheatCommonClientType ClientType;
	/** Remote user's platform, if known */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	EEOSEAntiCheatCommonClientPlatform ClientPlatform;
	/** 
	* Identifier for the remote user. This is typically a string representation of an
	* account ID, but it can be any string which is both unique (two different users will never
	* have the same string) and consistent (if the same user connects to this game session
	* twice, the same string will be used) in the scope of a single protected game session.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FString AccountId;
	/** 
	* Optional IP address for the remote user. May be null if not available.
	* IPv4 format: "0.0.0.0"
	* IPv6 format: "0:0:0:0:0:0:0:0"
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FString IpAddress;
public:
	explicit FEOSAntiCheatClientRegisterPeerOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_REGISTERPEER_API_LATEST)
		, PeerHandle(nullptr)
		, ClientType(EEOSEAntiCheatCommonClientType::EOS_ACCCT_UnprotectedClient)
		, ClientPlatform(EEOSEAntiCheatCommonClientPlatform::EOS_ACCCP_Unknown)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientUnregisterPeerOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Locally unique value describing the remote user, as previously passed to EOS_AntiCheatClient_RegisterPeer */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSAntiCheatCommonClientHandle PeerHandle;
public:
	explicit FEOSAntiCheatClientUnregisterPeerOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_UNREGISTERPEER_API_LATEST)
		, PeerHandle(nullptr)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatClientReceiveMessageFromPeerOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** The handle describing the sender of this message */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSAntiCheatCommonClientHandle PeerHandle;
	/** The size of the data received */
	uint32_t DataLengthBytes;
	/** The data received */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	TArray<uint8> Data;
public:
	explicit FEOSAntiCheatClientReceiveMessageFromPeerOptions()
		: ApiVersion(EOS_ANTICHEATCLIENT_RECEIVEMESSAGEFROMPEER_API_LATEST)
		, PeerHandle(nullptr)
		, DataLengthBytes(0)
	{
	}
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		DELEGATES
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAntiCheatClientOnMessageToServerCallback, const FEOSAntiCheatClientOnMessageToServerCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAntiCheatClientOnMessageToPeerCallback, const FEOSAntiCheatCommonOnMessageToClientCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAntiCheatClientOnPeerActionRequiredCallback, const FEOSAntiCheatCommonOnClientActionRequiredCallbackInfo&, data);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAntiCheatClientOnPeerAuthStatusChangedCallback, const FEOSAntiCheatCommonOnClientAuthStatusChangedCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAntiCheatClientOnMessageToServerCallbackDelegate, const FEOSAntiCheatClientOnMessageToServerCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAntiCheatClientOnMessageToPeerCallbackDelegate, const FEOSAntiCheatCommonOnMessageToClientCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAntiCheatClientOnPeerActionRequiredCallbackDelegate, const FEOSAntiCheatCommonOnClientActionRequiredCallbackInfo&, data);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAntiCheatClientOnPeerAuthStatusChangedCallbackDelegate, const FEOSAntiCheatCommonOnClientAuthStatusChangedCallbackInfo&, data);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		CALLBACK OBJECTS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
struct FAntiCheatClientMessageToServer
{
public:
	FAntiCheatClientMessageToServer(UObject* WorldContextObject, const FOnAntiCheatClientOnMessageToServerCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FAntiCheatClientMessageToServer()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAntiCheatClientOnMessageToServerCallback m_Callback;
};

struct FAntiCheatClientMessageToPeer
{
public:
	FAntiCheatClientMessageToPeer(UObject* WorldContextObject, const FOnAntiCheatClientOnMessageToPeerCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FAntiCheatClientMessageToPeer()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAntiCheatClientOnMessageToPeerCallback m_Callback;
};

struct FAntiCheatClientPeerActionRequired
{
public:
	FAntiCheatClientPeerActionRequired(UObject* WorldContextObject, const FOnAntiCheatClientOnPeerActionRequiredCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FAntiCheatClientPeerActionRequired()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAntiCheatClientOnPeerActionRequiredCallback m_Callback;
};

struct FAntiCheatClientPeerAuthStatusChanged
{
public:
	FAntiCheatClientPeerAuthStatusChanged(UObject* WorldContextObject, const FOnAntiCheatClientOnPeerAuthStatusChangedCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FAntiCheatClientPeerAuthStatusChanged()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAntiCheatClientOnPeerAuthStatusChangedCallback m_Callback;
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		Operations
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

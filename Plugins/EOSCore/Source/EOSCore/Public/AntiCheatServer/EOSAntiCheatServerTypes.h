/**
* Copyright (C) 2017-2021 | eelDev
*
* Official EOSCore Documentation: https://eeldev.com
*/

#pragma once

#include "CoreMinimal.h"
#include "eos_anticheatserver_types.h"
#include "Core/EOSCoreAntiCheatCommon.h"
#include "Core/EOSTypes.h"
#include "Core/EOSHelpers.h"
#include "EOSAntiCheatServerTypes.generated.h"

class UCoreAntiCheatServer;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		STRUCTS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerAddNotifyMessageToClientOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	explicit FEOSAntiCheatServerAddNotifyMessageToClientOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_ADDNOTIFYMESSAGETOCLIENT_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerAddNotifyClientActionRequiredOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	explicit FEOSAntiCheatServerAddNotifyClientActionRequiredOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_ADDNOTIFYCLIENTACTIONREQUIRED_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerAddNotifyClientAuthStatusChangedOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	explicit FEOSAntiCheatServerAddNotifyClientAuthStatusChangedOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_ADDNOTIFYCLIENTAUTHSTATUSCHANGED_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerBeginSessionOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** 
	* Time in seconds to allow newly registered clients to complete anti-cheat authentication.
	* Recommended value: 60
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 RegisterTimeoutSeconds;
	/** Optional name of this game server */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FString ServerName;
	/** 
	* Gameplay data collection APIs such as LogPlayerTick will be enabled if set to true.
	* If you do not use these APIs, it is more efficient to set this value to false.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	bool bEnableGameplayData;
	/** The Product User ID of the local user who is associated with this session. Dedicated servers should set this to null. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSProductUserId LocalUserId;
public:
	explicit FEOSAntiCheatServerBeginSessionOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_BEGINSESSION_API_LATEST)
		, RegisterTimeoutSeconds(60)
		, bEnableGameplayData(false)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerEndSessionOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	explicit FEOSAntiCheatServerEndSessionOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_ENDSESSION_API_LATEST)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerRegisterClientOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Locally unique value describing the remote user (e.g. a player object pointer) */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSAntiCheatCommonClientHandle ClientHandle;
	/** Type of remote user being registered */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	EEOSEAntiCheatCommonClientType ClientType;
	/** Remote user's platform, if known */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	EEOSEAntiCheatCommonClientPlatform ClientPlatform;
	/** 
	* Identifier for the remote user. This is typically a string representation of an
	* account ID, but it can be any string which is both unique (two different users will never
	* have the same string) and consistent (if the same user connects to this game session
	* twice, the same string will be used) in the scope of a single protected game session.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FString AccountId;
	/** 
	* Optional IP address for the remote user. May be null if not available.
	* IPv4 format: "0.0.0.0"
	* IPv6 format: "0:0:0:0:0:0:0:0"
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FString IpAddress;
public:
	explicit FEOSAntiCheatServerRegisterClientOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_REGISTERCLIENT_API_LATEST)
		, ClientHandle(nullptr)
		, ClientType(EEOSEAntiCheatCommonClientType::EOS_ACCCT_UnprotectedClient)
		, ClientPlatform(EEOSEAntiCheatCommonClientPlatform::EOS_ACCCP_Unknown)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerUnregisterClientOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Locally unique value describing the remote user, as previously passed to RegisterClient */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSAntiCheatCommonClientHandle ClientHandle;
public:
	explicit FEOSAntiCheatServerUnregisterClientOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_UNREGISTERCLIENT_API_LATEST)
		, ClientHandle(nullptr)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerReceiveMessageFromClientOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Optional value, if non-null then only messages addressed to this specific client will be returned */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSAntiCheatCommonClientHandle ClientHandle;
	/** The data received */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	TArray<uint8> Data;
public:
	explicit FEOSAntiCheatServerReceiveMessageFromClientOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_RECEIVEMESSAGEFROMCLIENT_API_LATEST)
		, ClientHandle(nullptr)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerSetClientNetworkStateOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Locally unique value describing the remote user (e.g. a player object pointer) */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSAntiCheatCommonClientHandle ClientHandle;
	/** True if the network is functioning normally, false if temporarily interrupted */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	bool bIsNetworkActive;
public:
	explicit FEOSAntiCheatServerSetClientNetworkStateOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_SETCLIENTNETWORKSTATE_API_LATEST)
		, ClientHandle(nullptr)
		, bIsNetworkActive(false)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerGetProtectMessageOutputLengthOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Length in bytes of input */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 DataLengthBytes;
public:
	explicit FEOSAntiCheatServerGetProtectMessageOutputLengthOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_GETPROTECTMESSAGEOUTPUTLENGTH_API_LATEST)
		, DataLengthBytes(0)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerProtectMessageOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Locally unique value describing the remote user to whom the message will be sent */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSAntiCheatCommonClientHandle ClientHandle;
	/** Length in bytes of input */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 DataLengthBytes;
	/** The data to encrypt */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	TArray<uint8> Data;
	/** The size in bytes of OutBuffer */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 OutBufferSizeBytes;
public:
	explicit FEOSAntiCheatServerProtectMessageOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_PROTECTMESSAGE_API_LATEST)
		, ClientHandle(nullptr)
		, DataLengthBytes(0)
		, OutBufferSizeBytes(0)
	{
	}
};

USTRUCT(BlueprintType)
struct FEOSAntiCheatServerUnprotectMessageOptions
{
	GENERATED_BODY()
public:
	/** Version of the API */
	int32 ApiVersion;
public:
	/** Locally unique value describing the remote user to whom the message will be sent */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	FEOSAntiCheatCommonClientHandle ClientHandle;
	/** Length in bytes of input */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 DataLengthBytes;
	/** The data to encrypt */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	TArray<uint8> Data;
	/** The size in bytes of OutBuffer */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AntiCheatClient")
	int32 OutBufferSizeBytes;
public:
	explicit FEOSAntiCheatServerUnprotectMessageOptions()
		: ApiVersion(EOS_ANTICHEATSERVER_UNPROTECTMESSAGE_API_LATEST)
		, ClientHandle(nullptr)
		, DataLengthBytes(0)
		, OutBufferSizeBytes(0)
	{
	}
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		DELEGATES
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
/**
* Callback issued when a new message must be dispatched to a connected client.
*
* Messages contain opaque binary data of up to 256 bytes and must be transmitted
* to the correct client using the game's own networking layer, then delivered
* to the client anti-cheat instance using the EOS_AntiCheatClient_ReceiveMessageFromServer function.
*
* This callback is always issued from within EOS_Platform_Tick on its calling thread.
*/
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAntiCheatServerOnMessageToClientCallback, const FEOSAntiCheatCommonOnMessageToClientCallbackInfo&, data);

/**
* Callback issued when an action must be applied to a connected client.
* This callback is always issued from within EOS_Platform_Tick on its calling thread.
*/
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAntiCheatServerOnClientActionRequiredCallback, const FEOSAntiCheatCommonOnClientActionRequiredCallbackInfo&, data);

/**
* Optional callback issued when a connected client's authentication status has changed.
* This callback is always issued from within EOS_Platform_Tick on its calling thread.
*/
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAntiCheatServerOnClientAuthStatusChangedCallback, const FEOSAntiCheatCommonOnClientAuthStatusChangedCallbackInfo&, data);

/**
* Callback issued when a new message must be dispatched to a connected client.
*
* Messages contain opaque binary data of up to 256 bytes and must be transmitted
* to the correct client using the game's own networking layer, then delivered
* to the client anti-cheat instance using the EOS_AntiCheatClient_ReceiveMessageFromServer function.
*
* This callback is always issued from within EOS_Platform_Tick on its calling thread.
*/
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAntiCheatServerOnMessageToClientCallbackDelegate, const FEOSAntiCheatCommonOnMessageToClientCallbackInfo&, data);

/**
* Callback issued when an action must be applied to a connected client.
* This callback is always issued from within EOS_Platform_Tick on its calling thread.
*/
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAntiCheatServerOnClientActionRequiredCallbackDelegate, const FEOSAntiCheatCommonOnClientActionRequiredCallbackInfo&, data);

/**
* Optional callback issued when a connected client's authentication status has changed.
* This callback is always issued from within EOS_Platform_Tick on its calling thread.
*/
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAntiCheatServerOnClientAuthStatusChangedCallbackDelegate, const FEOSAntiCheatCommonOnClientAuthStatusChangedCallbackInfo&, data);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		CALLBACK OBJECTS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
struct FAntiCheatServerMessageToClient
{
public:
	FAntiCheatServerMessageToClient(UObject* WorldContextObject, const FOnAntiCheatServerOnMessageToClientCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FAntiCheatServerMessageToClient()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAntiCheatServerOnMessageToClientCallback m_Callback;
};

struct FAntiCheatServerActionRequired
{
public:
	FAntiCheatServerActionRequired(UObject* WorldContextObject, const FOnAntiCheatServerOnClientActionRequiredCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FAntiCheatServerActionRequired()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAntiCheatServerOnClientActionRequiredCallback m_Callback;
};

struct FAntiCheatServerClientAuthStatusChanged
{
public:
	FAntiCheatServerClientAuthStatusChanged(UObject* WorldContextObject, const FOnAntiCheatServerOnClientAuthStatusChangedCallback& callback)
		: m_WorldContextObject(WorldContextObject)
		, m_Callback(callback)
	{
	}

	~FAntiCheatServerClientAuthStatusChanged()
	{
		m_Callback.Unbind();
	}

public:
	UObject* m_WorldContextObject;
	FOnAntiCheatServerOnClientAuthStatusChangedCallback m_Callback;
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
//		Operations
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
